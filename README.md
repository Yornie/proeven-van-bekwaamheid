# Beschrijving #
Welkom bij onze repository van de game Ben and Stan.
Het project is gemaakt in opdracht van Coolgames. Zij hebben de opdracht gegeven aan ons om een game te maken voor het nieuwe Facebook instant platform. Dit houd in dat we een game moeten maken met HTML5. Wij hebben de keuze gemaakt om het project te maken met typescript ( een superset van javascript ) in combinatie met de phaser engine. Het project zal gemaakt worden door 7 studenten van het mediacollege Amsterdam ( 2 programmeurs, 5 artists ).

## Notable scripts ##
* **PresetsSpawner :** [Link](https://bitbucket.org/Yornie/proeven-van-bekwaamheid/src/5973006188324c7dfe1035da2d4b281ce0fd3bfe/Ben&Stan/BenAndStan/PrefabSpawner.ts?at=master&fileviewer=file-view-default)
* **BackgroundController :** [Link](https://bitbucket.org/Yornie/proeven-van-bekwaamheid/src/5973006188324c7dfe1035da2d4b281ce0fd3bfe/Ben&Stan/BenAndStan/BackgroundController.ts?at=master&fileviewer=file-view-default)
* **PlayerController :** [Link](https://bitbucket.org/Yornie/proeven-van-bekwaamheid/src/5973006188324c7dfe1035da2d4b281ce0fd3bfe/Ben&Stan/BenAndStan/Player/PlayerController.ts?at=master&fileviewer=file-view-default)

## Links ##
* **Phaser:** [Phaser Homepage](http://www.phaser.io)
* **Google drive folder:** [Google Drive, Proeve van bekwaamheid](https://drive.google.com/drive/folders/0B9LRKEgG5n0HdmZJMk00MUxzN00?usp=sharing)
* **Technisch ontwerp:** [Google Doc, Technisch design](https://drive.google.com/open?id=0B8-kznRwMd1QZmlNcE5aQnZUbGs)
* **IDE document:** [Google Doc, IDE document](https://drive.google.com/open?id=0B8-kznRwMd1QMlRMWjQ3TnFjOFk)

## Software & IDE ##
* Game Engine: Phaser
* Code Editor: Visual Studio
* Code Language: Typescript
* Versioning: GIT, Bitbucket, SourceTree
* 2D Art: Photoshop, Illustrator
* 2D Animatie: DragonBones

# Team #
## Art ##
* Jelmer de Groen, Lead
* Romar de Boer
* Sara de Rooij
* Joey Pang
* Cherelle Drente

## Programming ##
* Yornie Westink, Lead
* Alessandro Bruin