﻿module BenAndStan
{
	/**
	 * The Goldcontroller controls the display of the gold text
	 */
    export class GoldController
    {
        private _currentGold: number = 0;
		private _game: Game;
        private _goldText: Phaser.Text;

		/**
		 * Constructs the Gold Controller
		 * @param game sets the _game variable according to the given parameter
		 */
		constructor(game: Game)
		{
			this._game = game;
			Game.goldController = this;
		}

		/**
		 * Creates the Gold Text which is displayed in the screen
		 */
		create()
		{
			this._goldText = this._game.add.text(this._game.world.bounds.right - 375, this._game.world.bounds.top + 75, 'Gold : 0', { fontSize: Constants.SCOREFONTSIZE, fill: '#e9e6d9', font: 'BigJohn' });
		}
		/**
		 * Adds gold to the Gold Total
		 * @param amount adds an 'amount' to the _currentGold variable
		 */
        public AddGold(amount: number): void
        {
            this._currentGold += amount;
            this._goldText.text = "Gold : " + this._currentGold;

        }
    }
}