﻿module BenAndStan
{
	/**
	 * The Score class mainly displays a distance meter where you can see how far you have traveled (flown)
	 */
	export class Score
	{
		private game: Game;
		private _score: number = 0;
		private _scoreString: string;

		public scoreText: Phaser.Text;
		/**
		 * Constructs the Score class
		 * @param _game sets the game varibable according to the given parameter
		 */
		constructor(_game: Game)
		{
			this.game = _game;
			Game.score = this;
		}
		/**
		 * Creates the score text which is displayed in screen
		 */
		public Create(): void
		{
			this.scoreText = this.game.add.text(this.game.world.bounds.right - 375, this.game.world.bounds.top + 25, 'Meters : ' + Game.score.ScoreMeters, { fontSize: Constants.SCOREFONTSIZE, fill: '#e9e6d9', font: 'BigJohn' });
		}
		/**
		 * Adds an amount of score to the total score
		 * @param amount adds an 'amount' to the _score variable
		 */
		public AddScore(amount: number): void
		{
			this._score += amount;
			this.scoreText.text = "Meters : " + this._score.toFixed(0);
		}
		/**
		 * Gets the score meter
		 */
		public ScoreMeters(): number
		{
			return this._score;
		}
	}
}
