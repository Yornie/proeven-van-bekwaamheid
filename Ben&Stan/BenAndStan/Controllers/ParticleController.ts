﻿module BenAndStan
{
	/**
	 * The ParticleController class manages particles.
	 */
	export class ParticleController
	{
		/**
		 * Constructs the Particle Controller
		 * @param _game sets the game variable according to the given parameter
		 */
		constructor(_game: Game)
		{
			this.game = _game;
			Game.particleController = this;
		}

		private game: Game;
		private particleEmitter: Phaser.Particles.Arcade.Emitter;

		/**
		 * Spawns particles with all its given values
		 * @param position position of the particles
		 * @param key string links to a sprite
		 * @param amount number of particles to spawn
		 * @param lifespan duration of the particles
		 * @param size size of the particles
		 * @param xSpeed speed of the particles on the X axis
		 * @param xOffSet amount of distance the particles spawn from the original position on the X axis
		 * @param yOffSet amoutn of distance the particles spawn from the original position on the Y axis
		 */
		public SpawnParticles(position: Phaser.Point, key: string, amount: number, lifespan: number, size?: Phaser.Point, xSpeed?: number, xOffSet?: number, yOffSet?: number)
		{
			if (xSpeed == undefined || xSpeed == null)
			{
				xSpeed = 0;
			}
			if (xOffSet == undefined || xOffSet == null)
			{
				xOffSet = 0;
			}
			if (yOffSet == undefined || yOffSet == null)
			{
				yOffSet = 0;
			}
			if (size == undefined || size == null)
			{
				size = new Phaser.Point(1, 1);
			}

			this.particleEmitter = this.game.add.emitter(0, 0, 10); //Adds emitter to the game
			this.particleEmitter.makeParticles(key); //Sets the particle sprite
			this.particleEmitter.gravity = 200; //Sets the particle gravity
			this.particleEmitter.particleBringToTop = true; //Sets the particle to the top layer
			this.particleEmitter.scale.set(size.x, size.y); // Scales the particles
			this.particleEmitter.position.x = position.x + xOffSet; //Sets the X position offset
			this.particleEmitter.position.y = position.y + yOffSet; //Sets the Y position offset
			var tween = this.game.add.tween(this.particleEmitter).to({ alpha: 0 }, 1000, "Linear", true, 0, -1, true); //Fades the particles away
			var tween = this.game.add.tween(this.particleEmitter.position).to({ x: position.x - xSpeed }, 1000, "Linear", true, 0, -1, true); //Sets speed for the particles
			this.particleEmitter.start(true, lifespan, null, amount); //Starts the emitter with all the given particle values above
		}
	}
}