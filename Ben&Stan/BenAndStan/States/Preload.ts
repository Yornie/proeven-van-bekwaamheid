﻿module BenAndStan
{
	/**
	 * The Preload class loads all the files (images files, Json files and Audio files)
	 */
    export class Preload extends Phaser.State
    {
        private _preloadBar: Phaser.Sprite;
		private _splash: Phaser.Sprite;

		/**
		 * Importing google fonts4
		 * Use the keys given below for the fonts
		 */
        WebFontConfig = {
            function() { this.game.time.events.add(Phaser.Timer.SECOND, this.create, this); },
            google: {
                families: ['ArcadeClassic', 'Big John', 'Slim Joe']
            }
        };

        preload()
        {
			this._splash = this.add.sprite(0, 0, 'splash');
			this._splash.width = this.game.world.width;
			this._splash.height = this.game.world.height;
            this._preloadBar = this.add.sprite(200, 250, 'preloadBar');
            this.load.setPreloadSprite(this._preloadBar);

			//Load all the functions that contain files that need to be loaded
			this.LoadAllImages();
			this.LoadFiles();
			this.LoadAudio();
            // Load google webfont loader
            this.game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
        }

		/**
		 * Load all the "Images" here
		 * Sprites/Spritesheets ect.
		 */
        private LoadAllImages()
        {
            //Placeholders
			this.game.load.image('boat', 'assets/ProtoStan.png'); //Placeholder boat
			this.game.load.image('blackPixel', 'assets/OneWhitePixel.png');

			//Boosts
			this.game.load.image('boost', 'assets/Boosts/Boost.png');
			this.game.load.image('hitter', 'assets/Boosts/Hitter.png');
			this.game.load.image('coinNet', 'assets/Boosts/Coinnet.png');
			this.game.load.image('coinMulti', 'assets/Boosts/Coinx2.png');
			this.game.load.image('coinMagnet', 'assets/Boosts/CoinMagnet.png');

			//Obstacles
			this.game.load.image('drone', 'assets/Obstacles/Drone.png');

			// --- Parallax objects --- \\
			/*
				the parallax is layered in 5 layers in total
				Going from -01 till 03 + a player layer and a main background
			*/
			// -- Lake -- \\
			//Main background
			this.game.load.image('skyMain', 'assets/Parallax/skylake_paralax_complete_background.png');
			//Start round phase objects
			this.game.load.image('bgHouse', 'assets/Parallax/startlevel_02_layer02.png');
			this.game.load.image('mainStart', 'assets/Parallax/startlevel_01_layer_playerline.png');
			this.game.load.image('mountain01', 'assets/Parallax/mountain_01_paralax_03.png');
			// Parallax 03
			this.game.load.image('mountain01', 'assets/Parallax/mountain_01_paralax_03.png');
			this.game.load.image('mountain02', 'assets/Parallax/mountain_02_paralax_03.png');
			this.game.load.image('mountain03', 'assets/Parallax/mountain_03_paralax_03.png');
			this.game.load.image('mountain04', 'assets/Parallax/mountain_04_paralax_03.png');
			this.game.load.image('mountain05', 'assets/Parallax/mountain_05_paralax_03.png');
			this.game.load.image('mountain06', 'assets/Parallax/mountain_06_paralax_03.png');
			// Parallax 02
			this.game.load.image('Object0102', 'assets/Parallax/boei_02_paralax_02.png');
			this.game.load.image('Object0202', 'assets/Parallax/island_small_paralax_layer_02.png');
			this.game.load.image('Object0302', 'assets/Parallax/islandbirdsign_small_paralax_layer_02.png');
			// Parallax 01
			this.game.load.image('Object0101', 'assets/Parallax/boei_01_paralax_01.png');
			this.game.load.image('Object0201', 'assets/Parallax/islandbirdsign_small_paralax_layer_01.png');
			this.game.load.image('Object0301', 'assets/Parallax/island_small_paralax_layer_01.png');
			this.game.load.image('Object0401', 'assets/Parallax/island_big_paralax_layer_01.png');

			// Parallax -01
			this.game.load.image('Object0100', 'assets/Parallax/forground_01_layer-01.png');
			this.game.load.image('Object0200', 'assets/Parallax/forground_02_layer-01.png');
			this.game.load.image('Object0300', 'assets/Parallax/forground_03_layer-01.png');
			this.game.load.image('Object0400', 'assets/Parallax/forground_04_layer-01.png');

            //Ingame Backgrounds
			this.game.load.image('backgroundStart', 'assets/backgrounds/lake_background_start.png');
            this.game.load.image('background1', 'assets/backgrounds/lake_background_01.png');
            this.game.load.image('background2', 'assets/backgrounds/lake_background_02.png');
            this.game.load.image('background3', 'assets/backgrounds/lake_background_03.png');
            this.game.load.image('background4', 'assets/backgrounds/lake_background_04.png');
            this.game.load.image('background5', 'assets/backgrounds/lake_background_05.png');
            this.game.load.image('background6', 'assets/backgrounds/lake_background_06.png');
            this.game.load.image('background7', 'assets/backgrounds/lake_background_07.png');
            this.game.load.image('background8', 'assets/backgrounds/lake_background_08.png');

            //Spritesheets
            this.game.load.spritesheet('player', 'assets/SpriteSheets/Stan_Idle_WIP.png', 389, 474, 30);  //Placeholder
            this.game.load.spritesheet('ben_fall_01', 'assets/SpriteSheets/ben_fall_01.png', 487, 510, 16);
            this.game.load.spritesheet('ben_fall_02', 'assets/SpriteSheets/ben_fall_02.png', 487, 510, 16);
            this.game.load.spritesheet('ben_fall_03', 'assets/SpriteSheets/ben_fall_03.png', 487, 510, 5);
            this.game.load.spritesheet('player_hit', 'assets/SpriteSheets/StanHITBAT.png', 463, 533, 30);
            this.game.load.spritesheet('coin', 'assets/SpriteSheets/Coin_Spritesheet.png', 54, 54);
            this.game.load.spritesheet('ben_hit_01', 'assets/SpriteSheets/ben_idle_bat_01.png', 314, 453, 24);
            this.game.load.spritesheet('ben_hit_02', 'assets/SpriteSheets/ben_idle_bat_02.png', 314, 453, 6);

            this.game.load.spritesheet('stan_selfie', 'assets/SpriteSheets/Stan_Selfie.png', 250, 112, 54);
            this.game.load.spritesheet('stan_startup', 'assets/SpriteSheets/Stan_Startup.png', 250, 112, 23);

			this.game.load.spritesheet('seagull_idle', 'assets/SpriteSheets/Seagull_Idle.png', 98, 108, 15);
            this.game.load.spritesheet('seagull_hit', 'assets/SpriteSheets/Seagull_Hit.png', 98, 108, 14);
            this.game.load.spritesheet('drone_idle', 'assets/SpriteSheets/Drone_Idle.png', 150, 83, 20);
            this.game.load.spritesheet('drone_hit', 'assets/SpriteSheets/Drone_Hit.png', 226, 241, 20);

			//VFX Spritesheets
			this.game.load.spritesheet('boat_foam01', 'assets/VFX/boat_foam_vfx_01.png', 720, 112, 41);
			this.game.load.spritesheet('boat_foam02', 'assets/VFX/boat_foam_vfx_02.png', 720, 112, 19);
			this.game.load.spritesheet('waterSplashVFX', 'assets/VFX/water_splash_vfx.png', 300, 400, 24);
			this.game.load.spritesheet('boost01VFX', 'assets/VFX/boost_pickup_big_01.png', 500, 500, 16);
			this.game.load.spritesheet('boost02VFX', 'assets/VFX/boost_pickup_big_02.png', 2048, 2048, 9);
			this.game.load.spritesheet('boostMidVFX', 'assets/VFX/boost_pickup_mid.png', 2048, 2048, 25); 
			this.game.load.spritesheet('boostSVFX', 'assets/VFX/boost_pickup_small.png', 2048, 2048, 25); 
			this.game.load.spritesheet('coinMagVFX', 'assets/VFX/coin_magnet_circle_vfx.png', 400, 400, 25); 
			this.game.load.spritesheet('coinPVFX', 'assets/VFX/coin_pickup_vfx.png', 54, 54, 28); 
			this.game.load.spritesheet('fallVFX', 'assets/VFX/fall_vfx.png', 300, 300, 18); 
            //UI
            //Main menu
            this.game.load.image('mainTitle', 'assets/main_menu/title.png');  //Title
            this.game.load.image('startButton', 'assets/main_menu/start_button.png');
            this.game.load.image('highButton', 'assets/main_menu/highscore_button.png');
            this.game.load.image('optionsButton', 'assets/main_menu/options_button.png');
            this.game.load.image('shopButton', 'assets/main_menu/shop_button.png');
            //Options
            this.game.load.image('optionsText', 'assets/option_menu/options_text.png');   //Title + Text
            this.game.load.image('backButton', 'assets/option_menu/back_button.png');
            this.game.load.image('volumeEmpty', 'assets/option_menu/volume_bar_empty.png');
            this.game.load.image('volumeFull', 'assets/option_menu/volume_bar_full.png');
            this.game.load.image('volumeHandle', 'assets/option_menu/volume_handle.png');
            //Highscores
            this.game.load.image('highscoreTitle', 'assets/highscore_menu/highscore_title.png'); //Title
            //pause screen
            this.game.load.image('pauseTitle', 'assets/pause_screen/bucketlist.png'); //Title
            this.game.load.image('screenshotButton', 'assets/pause_screen/camera_button.png');
            this.game.load.image('skipButton', 'assets/pause_screen/skip_button.png');
			this.game.load.image('pauseButton', 'assets/pause_screen/pause_button.png'); //Ingame pausebutton
            //UI Other
            this.game.load.image('title', 'assets/main_menu/title.png');
            this.game.load.image('filter', 'assets/main_menu/filter.png');
            this.game.load.image('pauseFilter', 'assets/pause_screen/filter_overlay.png');
		}

		/**
		 * Preload all the "Files" here
		 */
		private LoadFiles()
		{
			//Json Files
			this.game.load.tilemap('preset01', 'Data/Coin_preset_01.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset02', 'Data/Coin_preset_02.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset03', 'Data/Coin_preset_03.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset04', 'Data/Coin_preset_04.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset05', 'Data/Coin_preset_05.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset06', 'Data/Coin_preset_06.json', null, Phaser.Tilemap.TILED_JSON);

			this.game.load.tilemap('presetRW01', 'Data/risk_reward_01.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('presetRW02', 'Data/risk_reward_02.json', null, Phaser.Tilemap.TILED_JSON);

			this.game.load.tilemap('preset07', 'Data/Coin_preset_07.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset08', 'Data/Coin_preset_08.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset09', 'Data/Coin_preset_09.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset10', 'Data/Risk_reward_01.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset11', 'Data/Risk_reward_02.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset12', 'Data/Risk_reward_03.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset13', 'Data/Risk_reward_04.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset14', 'Data/Risk_preset_01.json', null, Phaser.Tilemap.TILED_JSON);

			this.game.load.tilemap('preset15', 'Data/4_drones.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset16', 'Data/seagull_curve.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset17', 'Data/yer_wizard_harry.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset18', 'Data/coinmagent_arrow.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset19', 'Data/coins_mid.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset20', 'Data/curving_synth.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset21', 'Data/seagulls_mid_coinmagnet_front.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset22', 'Data/twodrones_onecoin.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('preset23', 'Data/twoseagulls_onedrone_rightside_map.json', null, Phaser.Tilemap.TILED_JSON); 
			this.game.load.tilemap('preset24', 'Data/CoinEnemyBlock.json', null, Phaser.Tilemap.TILED_JSON);

		}

		/**
		 * Load all "Audio" files here
		 */
		private LoadAudio()
		{
			//Audio files
			this.game.load.audio('BGM_Sea', 'Audio/BGM_Sea.mp3');
			this.game.load.audio('click_sfx', 'Audio/click_sfx.wav');
			this.game.load.audio('coin_sfx', 'Audio/coin_sfx.wav');
			this.game.load.audio('death_ambient', 'Audio/death_ambient.mp3');
			this.game.load.audio('hit_sfx', 'Audio/hit_sfx.mp3');
			this.game.load.audio('jump_sfx', 'Audio/jump_sfx.wav');
			this.game.load.audio('powerup_sfx', 'Audio/powerup_sfx.wav');
		}

        create()
        {
            this.CreateLoadingBar();
        }
		
		/**
		 * Creating a loading bar
		 */
        private CreateLoadingBar()
        {
            var tween = this.add.tween(this._preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.FadeToGame, this);
        }

		/**
		 *  Fades the splash screen into the menu/game
		 */
        private FadeToGame()
        {
            var tween = this.add.tween(this._splash).to({ alpha: 0 }, 750, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.StartMainGame, this);
        }

		/**
		 * Start the menu stage
		 */
        private StartMainGame()
        {
            this.game.state.start("PrepMenu", true, false);
        }
    }
}