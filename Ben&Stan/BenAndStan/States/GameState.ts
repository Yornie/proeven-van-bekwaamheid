﻿module BenAndStan
{
	/**
	 * The main game world class Gamestate creates all the main classes
	 */
    export class GameState extends Phaser.State
    {
        private _objectPool: ObjectPool;
        private _playerController: PlayerController;
        private _background: Phaser.TileSprite;
        private _score: Score;
        private _goldController: GoldController;
        private _vehicleController: VehicleController;
		private _pauseMenu: PauseMenu;
		private _presetSpawner: PrefabSpawner;
		private _backgroundController: BackGroundController;
		private _particleController: ParticleController;
		
		private _blackPixelBot: Phaser.Sprite;

		/**
		 * Sets up the main classes for the game on entry of the main game
		 */
        preload()
		{
            this._objectPool = new ObjectPool(this.game);
            this._playerController = new PlayerController(this.game, 0, 0);
			this._score = new Score(this.game);
            this._vehicleController = new VehicleController(this.game, 0, 0);
			this._pauseMenu = new PauseMenu;
			this._presetSpawner = new PrefabSpawner(this.game);
			//TEMP
			this._backgroundController = new BackGroundController(this.game);			
        }
		
		/**
		 * Creates additional classes and starts certain classes
		 */
        create()
		{
			this.game.physics.startSystem(Phaser.Physics.ARCADE);
			this._playerController.CreatePlayer();
			this._vehicleController.Start(1100, 650);
			this._backgroundController.create();
			this._goldController = new GoldController(this.game);
			this._goldController.create();

            this._objectPool.CreateList();
			this._score.Create();
			this._particleController = new ParticleController(this.game);
        }

		/**
		 * Updates certain classes which need manual updating
		 */
        update()
		{
			this._backgroundController.MoveAllLayers();
			this._score.AddScore(0.2);
        }
    }
}