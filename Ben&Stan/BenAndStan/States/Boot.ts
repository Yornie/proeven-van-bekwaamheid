﻿module BenAndStan
{
	/**
	 * The Boot only loads in the splash and loading bar
	 */
    export class Boot extends Phaser.State
    {
		/**
		 * Preloads the loader for the loading menu
		 */
        preload()
        {
            this.load.image('preloadBar', 'assets/loader.png');
			this.load.image('splash', 'assets/Splash/level.jpg');
        }

        create()
        {
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;
            this.game.state.start('Preload');
        }
    }
}