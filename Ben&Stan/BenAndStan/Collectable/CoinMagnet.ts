﻿module BenAndStan
{
	/**
	 * The CoinMagnet class creates an object that you can pickup to draw nearby coins towards the player
	 */
	export class CoinMagnet extends Phaser.Sprite
	{
		private _magnetSprite: Phaser.Sprite;
        private _currentSpeed: number;
		private _objectPool: ObjectPool;
		private _maxVelocity: number;
		private _minVelocity: number;
		private _pickupVFX: Phaser.Sprite;
		private _enabled: boolean = true;
		
		/**
		 * Sets the starting values of the coin magnet
		 * @param x starting x position
		 * @param y starting y position
		 * @param objectPool sets the _objectPool variable according to the given parameter
		 */
		public Start(x: number, y: number, objectPool: ObjectPool): void
		{
			this._objectPool = objectPool;
			this._magnetSprite = this.game.add.sprite(x, y, 'coinMagnet');
			this.game.physics.arcade.enable(this._magnetSprite);
			this._magnetSprite.visible = true;
			this._magnetSprite.scale.setTo(0.75, 0.75);

			var oldBodyValues = new Array<number>();
			this._currentSpeed = -150;
			this._maxVelocity = 250;
			this._minVelocity = -250;

            this.game.add.existing(this);
        }
		/**
		 * Updates the coin
		 */
		public update(): void
		{
			if (this._enabled)
			{
				this._magnetSprite.body.velocity.x = -150;
				this.Bounce();
				this.game.physics.arcade.overlap(this._magnetSprite, Game.player.GetPlayerSprite(), this.Activate, null, this);
			} else 
			{
				this._objectPool.ToggleAttract(true);
				this._pickupVFX.position = Game.player.GetPlayerBody.position;
			}
		}
		/**
		 * Lets the magnet bounce up and down
		 */
		private Bounce()
		{
			var game = this.game;
			if (this._magnetSprite.position.y > game.world.centerY - 200)
			{
				this._currentSpeed -= 7.5;
			}
			if (this._magnetSprite.position.y < game.world.centerY + 200)
			{
				this._currentSpeed += 7.5;
			}

			if (this._currentSpeed >= this._maxVelocity)
			{
				this._currentSpeed = this._maxVelocity;
			} else if (this._currentSpeed <= this._minVelocity)
			{
				this._currentSpeed = this._minVelocity;
			}

			this._magnetSprite.body.velocity.y = this._currentSpeed;
		}

		/**
		 * Determines what it does on activation
		 * @param sprite sprite location
		 */
		private Activate(sprite): void
		{
			this.game.sound.play('powerup_sfx', 1, false);
			var pos: Phaser.Point = this._magnetSprite.position;

			/**
			*	Spawns the pickup VFX
			*/
			this._pickupVFX = this.game.add.sprite(pos.x, pos.y, 'coinMagVFX');
			this._pickupVFX.anchor.set(0.35, 0.35);
			this._pickupVFX.scale.set(0.8, 0.8);
			this._pickupVFX.animations.add('pickup', Phaser.ArrayUtils.numberArray(0, 25));
			this._pickupVFX.play('pickup', 30, true);

			this.game.time.events.add(Phaser.Timer.SECOND * 10, this.DisableMagnet, this);

			var tween = this.game.add.tween(this._magnetSprite.scale).to({ x: 0, y: 0 }, 500, "Linear", true, 0, 0,false);
			var tween = this.game.add.tween(this._magnetSprite).to({ alpha: 0 }, 1000, "Linear", true, 0, 0, false);
			var tween = this.game.add.tween(this._pickupVFX).to({ alpha: 0 }, 1000, "Linear", true, 9000, 0, false);
			this._enabled = false;
		}
		/**
		 * Disables the magnet
		 */
		private DisableMagnet(): void
		{
			this._objectPool.ToggleAttract(false);
			this.destroy();
		}
	}
}