﻿module BenAndStan
{
	/**
	 * BlockPowerUp is a class which is an powerup object which gives the player a boost when picked up
	 */
	export class BlockPowerUp extends Phaser.Sprite
	{
		/**
		* Power up variables
		*/
		private _blockSprite: Phaser.Sprite;
		private _pickupVFX: Phaser.Sprite;
        private _currentSpeed: number;
		private _maxVelocity: number;
		private _minVelocity: number;
		private _enabled: boolean = true;
		
		/**
		 * The Start function which sets the values of the PowerUp
		 * @param x starting X position
		 * @param y starting Y position
		 */
		public Start(x: number, y: number): void
		{
			this._blockSprite = this.game.add.sprite(x, y, 'hitter');
			this.game.physics.arcade.enable(this._blockSprite);
			this._blockSprite.visible = true;
			this._blockSprite.scale.setTo(0.75, 0.75);

			var oldBodyValues = new Array<number>();
			this._currentSpeed = -150;
			this._maxVelocity = 250;
			this._minVelocity = -250;

            this.game.add.existing(this);
        }
		
		/**
		 * Updates the powerup
		 */
		public update(): void
		{
			/**
			*	Checks whether it's enabled
			*/
			if (this._enabled)
			{
				this._blockSprite.body.velocity.x = -150;
				if (this._blockSprite.position.x + 50 < 0)
				{
					this._blockSprite.position.x = this.game.world.bounds.right + 50;
					this._blockSprite.position.y = 50 + Math.random() * (this.game.world.height - 50);
				}
				this.Bounce();
				this.game.physics.arcade.overlap(this._blockSprite, Game.player.GetPlayerSprite(), this.Activate, null, this);
			}
		}

		/**
		 * Lets the powerup bounce up and down
		 */
		private Bounce()
		{
			var game = this.game;
			if (this._blockSprite.position.y > game.world.centerY - 200)
			{
				this._currentSpeed -= 3.5;
			}
			if (this._blockSprite.position.y < game.world.centerY + 200)
			{
				this._currentSpeed += 3.5;
			}

			if (this._currentSpeed >= this._maxVelocity)
			{
				this._currentSpeed = this._maxVelocity;
			} else if (this._currentSpeed <= this._minVelocity)
			{
				this._currentSpeed = this._minVelocity;
			}

			this._blockSprite.body.velocity.y = this._currentSpeed;
		}
		/**
		 * Determines what the powerup does and spawns particles
		 * @param sprite sprite location
		 */
		private Activate(sprite): void
		{
			this.game.sound.play('powerup_sfx', 1, false);
			this._enabled = false;
			var pos: Phaser.Point = this._blockSprite.position;
			this._pickupVFX = this.game.add.sprite(pos.x, pos.y, 'boost01VFX');
			this._pickupVFX.anchor.set(0.5, 0.5);
			this._pickupVFX.scale.set(0.75, 0.75);
			this._pickupVFX.animations.add('pickup', Phaser.ArrayUtils.numberArray(0, 16));
			this._pickupVFX.play('pickup', 30, false);

			var tween = this.game.add.tween(this._blockSprite.scale).to({ x: 0, y: 0 }, 500, "Linear", true, 0, -1, true);
			var tween = this.game.add.tween(this._blockSprite).to({ alpha: 0 }, 1000, "Linear", true, 0, -1, true);
			var tween = this.game.add.tween(this._pickupVFX).to({ alpha: 0 }, 1600, "Linear", true, 0, -1, true);
			var tween = this.game.add.tween(this._pickupVFX.position).to({ x: pos.x - 150 }, 2000, "Linear", true, 0, -1, true);
			this.game.time.events.add(Phaser.Timer.SECOND * 2, this.DestroyThis, this);
			Game.player.EnableBlock();
		}

		/**
		 * Destroys the powerup completely
		 */
		private DestroyThis(): void
		{
			this._pickupVFX.kill();
			this._blockSprite.kill();
			this.destroy();
		}
	}
}