﻿module BenAndStan
{
	/**
	 * The basic Coin class which is an pickupable object that adds score when picked up
	 */
	export class Coin extends Phaser.Sprite
	{
		private _coinSprite: Phaser.Sprite;
		private _pickupVFX: Phaser.Sprite;
		private _playerSprite: Phaser.Sprite;

		private _currentSpeed: number;
		private _canAttract: boolean = false;
		private _random: boolean = false;
		private _spawnPresetOnEnter: boolean = false;
		private _enabled: boolean = true;
		
		/**
		 * Sets the values of the Coin
		 * @param x starting X position
		 * @param y starting Y position
		 */
		public Start(x: number, y: number, random?: boolean): void
		{
			this._random = random;
			this._playerSprite = Game.player.GetPlayerSprite();
			this._coinSprite = this.game.add.sprite(x, y, 'coin');
			this.game.physics.arcade.enable(this._coinSprite);
			this._coinSprite.visible = true;
			this._coinSprite.anchor.set(0.5, 0.5); 
			this._coinSprite.animations.add('coinspin', Phaser.ArrayUtils.numberArray(0, 16), 16);
			this._coinSprite.play('coinspin', 16, true);
            this._currentSpeed = -150;
            this.game.add.existing(this);
			Game.objectPool.AddCoin(this);
		}

		/**
		 * Updates the Coin
		 */
		public update(): void
		{
			if (this._enabled)
			{
				if ((this._spawnPresetOnEnter) && this._coinSprite.position.x < this.game.world.centerX)
				{
					this._spawnPresetOnEnter = false;
					Game.prefabSpawner.spawnPreset();
				}
				this._coinSprite.body.velocity.x = this._currentSpeed;
				this.game.physics.arcade.overlap(this._coinSprite, this._playerSprite, this.AddScore, null, this);

				if (this._coinSprite.position.x + 50 < 0)
				{
						this._coinSprite.kill();
						this.destroy();
				}
				if (this._canAttract)
				{
					this.Attract();
				} else if (this._coinSprite.body.velocity.y != 0)
				{
					this._coinSprite.body.velocity.y = 0;
				}
			}
		}
		/**
		 * Adds score on pickup
		 */
		private AddScore(): void
		{
			this._enabled = false;
			this._currentSpeed = -150;
			
			var pos: Phaser.Point = this._coinSprite.position;
			
			/**
			* Spawns the pickup VFX
			*/
			this._pickupVFX = this.game.add.sprite(pos.x, pos.y, 'coinPVFX');
			this._pickupVFX.anchor.set(0.5, 0.5);
			this._pickupVFX.scale.set(1.2, 1.2);
			this._pickupVFX.animations.add('pickup', Phaser.ArrayUtils.numberArray(0, 28));
			this._pickupVFX.play('pickup', 30, false);

			/**
			 * Fades the coin and moves the VFX
			 */
			var tween = this.game.add.tween(this._coinSprite.scale).to({ x: 0, y : 0 }, 500, "Linear", true, 0, -1, true);
			var tween = this.game.add.tween(this._coinSprite).to({ alpha: 0 }, 1000, "Linear", true, 0, -1, true);
			var tween = this.game.add.tween(this._pickupVFX.position).to({ x: pos.x - 150 }, 2000, "Linear", true, 0, -1, true);
			this.game.time.events.add(Phaser.Timer.SECOND * 2, this.DestroyThis, this);

			this.game.sound.play('coin_sfx', 0.1, false);
			Game.goldController.AddGold(Constants.COIN_VALUE);
		}
		/**
		 * Destroys the Coin completely
		 */
		private DestroyThis(): void
		{
			this._pickupVFX.kill();
			this._pickupVFX.destroy();
			this._coinSprite.kill();			
			this.destroy();
		}
		
		/**
		 * Toggles the attract boolean
		 * @param bool sets the _canAttract boolean according to the given parameter boolean
		 */
		public ToggleAttract(bool : boolean): void
		{
			this._canAttract = bool;
		}

		/**
		 * Gets the Attract boolelan
		 */
		public AttractEnabled(): boolean
		{
			return this._canAttract;
		}
		/**
		 * Enables the next preset spawn
		 */
		public EnableSpawnOnDestroy(): void
		{
			this._spawnPresetOnEnter = true;
		}
		/**
		 * Attracts to coin towards the player if in a certain range
		 */
		public Attract(): void
		{
			var dist: number;
			dist = this._coinSprite.position.distance(this._playerSprite.position);
			if (this._playerSprite.position.x < this._coinSprite.position.x && dist < 250)
			{
				this._currentSpeed = -200;
				if (this._playerSprite.position.y - 5 > this._coinSprite.position.y)
				{
					this._coinSprite.body.velocity.y = 100;
				}
				else if (this._playerSprite.position.y + 5 < this._coinSprite.position.y)
				{
					this._coinSprite.body.velocity.y = -100;
				} else
				{
					this._coinSprite.body.velocity.y = 0;
				}
			} else
			{
				this._coinSprite.body.velocity.y = 0;
				this._currentSpeed = -150;
			}
		}
	}
}