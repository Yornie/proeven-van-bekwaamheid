﻿module BenAndStan
{
	/**
	 * This class controls the background parallax images.
	 */
	export class BackGroundController
	{
		private _game: Game;
		private _backgroundLayer: Phaser.Group;

		private _mapStrings: string[];

		// Arrays 
		private _frontStrings: string[];
		private _firstStrings: string[];
		private _secondStrings: string[];
		private _thirdStrings: string[];
		//Groups
		private _frontLayer: Phaser.Group;
		private _playerLayer: Phaser.Group;
		private _firstLayer: Phaser.Group;
		private _secondLayer: Phaser.Group;
		private _thirdLayer: Phaser.Group;
		private _mainGroup: Phaser.Group;	//background group
		   
		private _startScreen: Phaser.Sprite;
		private _startBackScreen: Phaser.Sprite;

		private _frontPara: Phaser.Sprite;	//0
		private _firstPara: Phaser.Sprite;
		private _secondPara: Phaser.Sprite;	//2
		private _thirdPara: Phaser.Sprite; //3
		private _background: Phaser.Sprite; //static background

		private _playerSprite: Phaser.Sprite;
		private _vehicleSprite: Phaser.Sprite;
		private _vehicleVFX: Phaser.Sprite;

		/**
		 * Constructs the Background Controller
		 * @param game set the _game variable according to the given parameter
		 */
		constructor(game : Game)
		{
			this._game = game;
		}

		/**
		 * Creates the Backgrounds Controller
		 * Creates group layers
		 * Adding all the parallax key strings to the corresponding arrays
		 */
		create()
		{
			//Creating the layers for the parallax and adding it to 1 main group
			//The groups are orderd from the back to the front layer
			this._mainGroup = this._game.add.group();
			this._thirdLayer = this._game.add.group();
			this._secondLayer = this._game.add.group();
			this._firstLayer = this._game.add.group();
			this._playerLayer = this._game.add.group();
			this._frontLayer = this._game.add.group();
			
			//Setting layer Y position
			this._secondLayer.position.y = this._game.world.centerY;
			this._thirdLayer.position.y = this._game.world.centerY;
			this._firstLayer.position.y = this._game.world.centerY;
			this._frontLayer.position.y = this._game.world.centerY;
			this._mainGroup.pivot.set(0.5, 0.5);
			//setting layer X start positions
			this._secondLayer.position.x = this._game.world.centerX;
			this._thirdLayer.position.x = this._game.world.centerX;
			this._firstLayer.position.x = this._game.world.centerX;
			this._frontLayer.position.x = this._game.world.centerX;

			//Creating the arrays
			//front layer strings
			this._frontStrings = new Array<string>();
			this._frontStrings.push('Object0100');
			this._frontStrings.push('Object0200');
			this._frontStrings.push('Object0300');
			this._frontStrings.push('Object0400');
			//first layer strings
			this._firstStrings = new Array<string>();
			this._firstStrings.push('Object0101');
			this._firstStrings.push('Object0201');
			this._firstStrings.push('Object0301');
			this._firstStrings.push('Object0401');

			//second layer strings
			this._secondStrings = new Array<string>();
			this._secondStrings.push('Object0102');
			this._secondStrings.push('Object0202');
			this._secondStrings.push('Object0302');
			//third layer strings
			this._thirdStrings = new Array<string>();
			this._thirdStrings.push('mountain01');
			this._thirdStrings.push('mountain02');
			//this._thirdStrings.push('mountain04');
			this._thirdStrings.push('mountain05');
			//this._thirdStrings.push('mountain06');

			//adding the player sprite to the playerlayer
			this._playerLayer.add(Game.player.GetPlayerSprite());
			this._playerLayer.add(Game.player.FallVFX());
			this._playerLayer.add(Game.vehicle.VehicleSprite());
			this._playerLayer.add(Game.vehicle.VehicleVFX());
			this._playerLayer.add(Game.vehicle.VehicleLine());

			//Creating the first objects for the parallax
			this._background = this._mainGroup.create(this._game.world.centerX, this._game.world.centerY, 'skyMain');
			this._frontPara = this._frontLayer.create(this._game.world.bounds.right + this._game.world.centerX, Constants.FRONTLAYER_Y, this._frontStrings[Math.floor(Math.random() * this._frontStrings.length)]);
			this._firstPara = this._firstLayer.create(this._game.world.bounds.right + this._game.world.centerX, Constants.FIRSTLAYER_Y, this._firstStrings[Math.floor(Math.random() * this._firstStrings.length)]);
			this._secondPara = this._secondLayer.create(this._game.world.bounds.right + this._game.world.centerX, Constants.SECONDLAYER_Y, this._secondStrings[Math.floor(Math.random() * this._secondLayer.length)]);
			this._thirdPara = this._thirdLayer.create(this._game.world.bounds.right + this._game.world.centerX, Constants.THIRDLAYER_Y, this._thirdStrings[Math.floor(Math.random() * this._thirdStrings.length)]);
			this._startScreen = this._firstLayer.create(this._game.world.bounds.left + this._game.world.centerX / 9.5, -100, 'mainStart');
			this._startBackScreen = this._secondLayer.create(this._game.world.left * 7, -15, 'bgHouse');

			//setting pivots
			this._startScreen.pivot.set(0, 1);
			this._frontPara.pivot.set(0, 1);
			this._firstPara.pivot.set(0, 1);
			this._secondPara.pivot.set(0, 1);
			this._thirdPara.pivot.set(0, 1);
			//setting anchors
			this._frontPara.anchor.set(0.5, 0.5);
			this._firstPara.anchor.set(0.5, 0.5);
			this._secondPara.anchor.set(0.5, 0.5);
			this._thirdPara.anchor.set(0.5, 0.5);
			this._startScreen.anchor.set(0.5, 0.5);
			this._background.anchor.set(0.5, 0.5);
		}

		/**
		 * Move layers + checks if the location of the parallax is past x-value and sets them back and gets a new key from the array
		 */
		MoveAllLayers(): void
		{
			this._frontPara.position.x -= Constants.FRONTLAYER_SPEED;
			this._firstPara.position.x -= Constants.FIRSTLAYER_SPEED;
			this._secondPara.position.x -= Constants.SECONDLAYER_SPEED;
			this._thirdPara.position.x -= Constants.THIRDLAYER_SPEED;

			//Start
			if (this._startScreen.position.x >= this._game.world.bounds.left + this._startScreen.width * 2.5 && this._startBackScreen.position.x >= this._game.world.bounds.left + this._startBackScreen.width * 2.5)
			{
				this._startScreen.destroy();
				this._startBackScreen.destroy();
			} else {
				this._startScreen.position.x -= Constants.FIRSTLAYER_SPEED;
				this._startBackScreen.position.x -= Constants.SECONDLAYER_SPEED;
			}
			
			//FrontLayer
			if (this._frontPara.position.x <= this._game.world.bounds.left - this._frontPara.width * 2.5)
			{
				this._frontPara.loadTexture(this._frontStrings[Math.floor(Math.random() * this._frontStrings.length)]);
				this._frontPara.pivot.set(0, 1);
				this._frontPara.position.x = this._game.world.bounds.right + this._frontPara.width;
			} 
			//firstLayer
			if (this._firstPara.position.x <= this._game.world.bounds.left - this._firstPara.width * 6)
			{
				this._firstPara.loadTexture(this._firstStrings[Math.floor(Math.random() * this._firstStrings.length)]);
				this._firstPara.pivot.set(0, 1);
				this._firstPara.position.x = this._game.world.bounds.right + this._firstPara.width;
			} 
			//Secondlayer
			if (this._secondPara.position.x <= this._game.world.bounds.left - this._secondPara.width * 15 )
			{
				this._secondPara.loadTexture(this._secondStrings[Math.floor(Math.random() * this._secondStrings.length)]);
				this._secondPara.pivot.set(0, 1);
				this._secondPara.position.x = this._game.world.bounds.right + this._secondPara.width;
			}
			//ThridLayer
			if (this._thirdPara.position.x <= this._game.world.bounds.left - this._thirdPara.width * 2.5)
			{
				this._thirdPara.loadTexture(this._thirdStrings[Math.floor(Math.random() * this._thirdStrings.length)]);
				this._thirdPara.pivot.set(0, 1);
				this._thirdPara.position.x = this._game.world.bounds.right + this._game.world.centerX;
			}
		}		
	}
}