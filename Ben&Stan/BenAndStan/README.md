# Ben And Stan#

## Beschrijving ##

 Dit is ons project Ben And Stan. Dit is ons Proeve van Bekwaamheid project geworden dat word gemaakt
 door 4e jaars Game-Artists en Game-Developers. De bedoeling is om een product neer te zetten voor een klant die
 ons is toegewezen en een aantal eisen had. Het product moet op HTML5 kunnen afspelen, speelbaar zijn op
 de computer en de mobiele telefoon via Facebook Instant.

## Links ##

* **Phaser:** [Phaser Homepage](https://phaser.io)
* **Google drive folder:** [Google Drive, Proeve van bekwaamheid](https://drive.google.com/open?id=0B9LRKEgG5n0HdmZJMk00MUxzN00)
* **Technisch ontwerp:** [Google Doc, Technisch design](https://docs.google.com/document/d/1-FORw8-wg_9rN4Wg61F1RNpT2b0Wt-beupofIpwInwQ/edit?usp=sharing)
* **IDE document:** [Google Doc, IDE document](https://docs.google.com/document/d/1ZW-WjytziXlqZjpnO0CyQVK1CwrUdUZYtUel_HPJnw8/edit?usp=sharing)
* **Bitbucket:** [Repository](https://bitbucket.org/Yornie/proeven-van-bekwaamheid)

## Software & IDE ##

* **Game Engine:** Phaser
* **Code Editor:** Visual Studio
* **Code Language:** Typescript
* **Versioning:** GIT, Bitbucket, SourceTree
* **2D Art:** Photoshop, Illustrator
* **2D Animatie:** [DragonBones](http://dragonbones.com/en/index.html#.WPoLJfnyhhE)

## Team ##
#### Art ####
* Jelmer de Groen, **Lead**
* Romar de Boer
* Sara de Rooij
* Joey Pang
* Cherelle Drente

#### Programming ####
* Yornie Westink, **Lead**
* Alessandro Bruin