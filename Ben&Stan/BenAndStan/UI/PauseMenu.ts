﻿module BenAndStan
{
	/**
	 * The PauseMenu class controls the game so that the game can be paused.
	 */
	export class PauseMenu
	{
		private _filter: Phaser.TileSprite;
		private _menuButton: Phaser.Button;
		private _resumeButton: Phaser.Button;
		private _pauseTitle: Phaser.Sprite;
		private _ChallengeT1: string;

		private _pauseGroup: Phaser.Group;

		private _pauseButton: Phaser.Button;
		private game: Game;

		/**
		 * Creates the Pause Menu
		 */
		create()
		{
			//Create Button
			this._pauseButton = this.game.add.button(
				this.game.world.centerX,
				this.game.world.centerY,
				'pauseButton', this.PauseGame, this);
			//Create pause screen		
			// CHANGE FOR Objectpool
			this._filter = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.height, 'pauseFilter');
			this._pauseTitle = this.game.add.sprite(this._filter.x, this._filter.y - 20, 'pauseTitle');
			this._menuButton = this.game.add.button(this._filter.x - 5, this._filter.y, 'skipButton'); //Placeholder art

			//Anchors
			

			//Adding to group
			this._pauseGroup.add(this._filter);
			this._pauseGroup.add(this._menuButton);
			this._pauseGroup.add(this._pauseTitle);
			this._pauseGroup.add(this._resumeButton);
		}

		/**
		 * Pauses the game
		 */
		PauseGame()
		{
			this._pauseButton.visible = false;
			this.SlideIn();
		}

		/**
		 * Slides the menu in
		 */
		SlideIn()
		{
			var bounce = this.game.add.tween(this._pauseGroup);
			bounce.to(this.game.width, 3, Phaser.Easing.Bounce.In);
		}

		/**
		 * Slides the menu out
		 */
		SlideOut()
		{
			var bounce = this.game.add.tween(this._pauseGroup);
			bounce.to(this.game.width, 3, Phaser.Easing.Bounce.In);
		}
		
	}
}