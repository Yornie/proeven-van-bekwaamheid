﻿module BenAndStan
{
	/**
	 * The PrepMenu is the main menu where you can start the game from here.
	 */
    export class PrepMenu extends Phaser.State
    {
        private _startButton: Phaser.Button;
        private _highscoreButton: Phaser.Button;
        private _optionsButton: Phaser.Button;
        private _filter: Phaser.TileSprite;
        private _background: Phaser.TileSprite;
		private _title: Phaser.Sprite;

		private _fadeCompleted = false;

		/**
		 * Creates the main Menu
		 */
        create()
        {
			this._background = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'background2');
			this._filter = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'filter');
            this._title = this.game.add.sprite(this.game.world.centerX - 436, this.world.centerY - 350, 'mainTitle');
            
            //Setting Buttons
            this._startButton = this.game.add.button(
                this.game.world.centerX - 110,
                this.game.world.centerY - 70,
                'startButton', this.StartGame, this);
            this._highscoreButton = this.game.add.button(
                this.game.world.centerX - 200,
                this.game.world.centerY + 30,
                'highButton', this.ShowHighscore, this, this._highscoreButton, this._optionsButton);
            this._optionsButton = this.game.add.button(
                this.game.world.centerX - 152,
                this.game.world.centerY + 120,
                'optionsButton', this.ShowOptions, this);

			
			this._background.alpha = 0;
			var tween = this.game.add.tween(this._background).to({ alpha: 1 }, 750, Phaser.Easing.Linear.None, true);
			tween.onComplete.add(this.EnabledFadeCompleted, this);

			this._filter.alpha = 0;
			var tween = this.game.add.tween(this._filter).to({ alpha: 1 }, 750, Phaser.Easing.Linear.None, true);

			this._title.alpha = 0;
			var tween = this.game.add.tween(this._title).to({ alpha: 1 }, 750, Phaser.Easing.Linear.None, true);

			this._startButton.alpha = 0;
			var tween = this.game.add.tween(this._startButton).to({ alpha: 1 }, 750, Phaser.Easing.Linear.None, true);

			this._highscoreButton.alpha = 0;
			var tween = this.game.add.tween(this._highscoreButton).to({ alpha: 1 }, 750, Phaser.Easing.Linear.None, true);

			this._optionsButton.alpha = 0;
			var tween = this.game.add.tween(this._optionsButton).to({ alpha: 1 }, 750, Phaser.Easing.Linear.None, true);
			this.game.sound.play('BGM_Sea', 1, true);
        }

		/**
		 * Enables the FadeCompleted boolean when the menu finished its fade
		 */
		EnabledFadeCompleted()
		{
			this._fadeCompleted = true;
		}

		/**
		 * Starts the main game world on start button press
		 */
        StartGame()
        {
			if (this._fadeCompleted)
			{
				this.game.sound.play('click_sfx', 1, false);
				this.game.state.start("GameState");
			}
        }

		ButtonBlink()
		{
			
		}

        ShowHighscore()
        {

        }
        ShowOptions()
        {

        }
    
    }
}