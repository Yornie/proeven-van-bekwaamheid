﻿module BenAndStan
{
	export class PrefabSpawner
	{
		private _map: Phaser.Tilemap;
		private _game: Game;
		private _presetArray: string[];

		/*
		<---- Json values ---->
		Objectlayer = ObjectLayer
		Coins : "coin"
		Seagull : "enemy_seagull"
		Drone: "enemy_drone"
		Fish: "enemy_fish"
		Coin Magnet : "powerup_coinMagnet
		*/

		/**
		 * Construct the prefab spawner
		 * @param game sets the _game variable according to the given parameter
		 */
		constructor(game : Game)
		{
			this._game = game;
			Game.prefabSpawner = this;

			if (!this._presetArray)
			{
				//Add the string from the Json file here
				this._presetArray = new Array<string>();
				//this._presetArray.push('preset01');
				this._presetArray.push('preset02');
				this._presetArray.push('preset03');
				this._presetArray.push('preset04');
				this._presetArray.push('preset05');
				//this._presetArray.push('preset06');
				this._presetArray.push('preset07');
				this._presetArray.push('preset08');
				this._presetArray.push('preset09'); 
				//this._presetArray.push('preset10');
				this._presetArray.push('preset11');
				this._presetArray.push('preset12');
				this._presetArray.push('preset13');
				this._presetArray.push('preset14');
				this._presetArray.push('preset15');
				this._presetArray.push('preset16');
				this._presetArray.push('preset17');
				this._presetArray.push('preset18');
				this._presetArray.push('preset19');
				this._presetArray.push('preset20');
				this._presetArray.push('preset21');
				this._presetArray.push('preset22');
				this._presetArray.push('preset23');
				this._presetArray.push('preset24');
			}
		}

		/**
		 * Spawns presets
		 * Checks all the objects in the given tilemap
		 * and sets the objects according to the string from the tilemap
		 */
		public spawnPreset()
		{
			//Setting local vars
			var presetArray = this._presetArray;
			var gameV = this._game;
			//Gets a random preset string
			var randomPreset = presetArray[Math.floor(Math.random() * presetArray.length)];
			this._map = gameV.add.tilemap(randomPreset);
			//Checks the Objectlayer for the following values, spawns them accordingly 
			var lastObjec;
			console.log(randomPreset);
			this._map.objects["ObjectLayer"].forEach(function (value)
			{
				console.log(randomPreset + " | " + value.type);
				//Coins
				if (value.type === "coin")
				{
					var coin = new Coin(gameV, 0, 0);
					coin.Start(gameV.world.bounds.right + value.x, value.y);
					lastObjec = coin;
				}
				//Seagull enemies
				if (value.type === "enemy_seagull")
				{
					var seagull = new EnemyMover(gameV, 0, 0);
					seagull.Start(gameV.world.bounds.right + value.x, value.y);
					lastObjec = seagull;
				}
				//Drone
				if (value.type === "enemy_drone")
				{
					var drone = new EnemyDrone(gameV, 0, 0);
					drone.Start(gameV.world.bounds.right + value.x, value.y);
					lastObjec = drone;
				}
				//Coin Magnet powerup
				if (value.type === "powerup_coinMagnet")
				{
					var coinMag = new CoinMagnet(gameV, 0, 0);
					coinMag.Start(gameV.world.bounds.right + value.x, value.y, Game.objectPool);
				}
				//Block powerup
				if (value.type === "powerup_block")
				{
					var bat = new BlockPowerUp(gameV, 0, 0);
					bat.Start(gameV.world.bounds.right + value.x, value.y);
				}
			});
			console.log(randomPreset);
			lastObjec.EnableSpawnOnDestroy(); //Enables next preset spawn on last object
		}		
	}
}