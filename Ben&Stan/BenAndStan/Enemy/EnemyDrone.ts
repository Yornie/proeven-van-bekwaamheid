﻿module BenAndStan
{
	/**
	 * The EnemyDrone class is an object class which the player must dodge
	 */
	export class EnemyDrone extends Phaser.Sprite
	{
		private _enemySprite: Phaser.Sprite;
		private _currentVelocity: number;
		private _maxVelocity: number;
		private _minVelocity: number;
		private _spawnPresetOnEnter: boolean = false;
		private _enabled: boolean = true;
		private _centerY: number;
		/**
		 * Starts the EnemyDrone and sets it's values
		 * @param x starting x position
		 * @param y starting y position
		 */
		public Start(x: number, y: number): void
		{
			this._enemySprite = this.game.add.sprite(x, y, 'drone_idle');
			this.game.physics.arcade.enable(this._enemySprite);
			this._enemySprite.visible = true;

			this._enemySprite.anchor.set(0.5, 0.5);
			this._enemySprite.scale.setTo(0.75, 0.75);

			this._enemySprite.animations.add('idle', Phaser.ArrayUtils.numberArray(0, 20));
			this._enemySprite.play('idle', 30, true);

			var oldBodyValues = new Array<number>();

			oldBodyValues.push(this._enemySprite.body.width);
			oldBodyValues.push(this._enemySprite.body.height);
			this._enemySprite.body.setSize(oldBodyValues[0] / 1.5, oldBodyValues[1], 20, 0); //Changes it's hitbox

			this._centerY = y;

			this._currentVelocity = 250;
			this._maxVelocity = 250;
			this._minVelocity = -250;
            this.game.add.existing(this);
		}

		/**
		 * Updates the Enemy Drone
		 */
		public update(): void
		{
			if (this._enabled)
			{
				if ((this._spawnPresetOnEnter) && this._enemySprite.position.x < this.game.world.centerX)
				{
					this._spawnPresetOnEnter = false;
					Game.prefabSpawner.spawnPreset();
				}

				this._enemySprite.body.velocity.x = -150;
				this.Bounce();
				this.game.physics.arcade.overlap(this._enemySprite, Game.player.GetPlayerSprite(), this.OnPlayerCollision, null, this);
				if (this._enemySprite.position.x < 0)
				{
					this.destroy();
				}
			}
		}
		/**
		 * Lets the Enemy Drone bounce up and down
		 */
		private Bounce()
		{
			if (this._enemySprite.position.y > this._centerY)
			{
				this._currentVelocity -= 7.5;
			}
			if (this._enemySprite.position.y < this._centerY)
			{
				this._currentVelocity += 7.5;
			}

			if (this._currentVelocity >= this._maxVelocity)
			{
				this._currentVelocity = this._maxVelocity;
			} else if (this._currentVelocity <= this._minVelocity)
			{
				this._currentVelocity = this._minVelocity;
			}

			this._enemySprite.body.velocity.y = this._currentVelocity;
		}
		/**
		 * Enables next preset spawn
		 */
		public EnableSpawnOnDestroy(): void
		{
			this._spawnPresetOnEnter = true;
		}
		/**
		 * Determines what it does on Collision with the Player
		 * @param sprite sprite location
		 */
		private OnPlayerCollision(sprite): void
		{
			if (Game.player.GetBlockActivated) //Checks if the player can block
			{
				var pos: Phaser.Point = this._enemySprite.position;
				this._enemySprite.visible = false;
				this._enemySprite = this.game.add.sprite(pos.x, pos.y, 'drone_hit'); //Changes sprite
				this._enemySprite.visible = true;
				this._enemySprite.anchor.set(0.5, 0.5);
				this._enemySprite.scale.setTo(0.75, 0.75);
				this._enemySprite.animations.add('hit', Phaser.ArrayUtils.numberArray(0, 20));
				this._enemySprite.play('hit', 30, false);
				this.game.time.events.add(Phaser.Timer.SECOND * 2, this.DestroyThis, this);
				var tween = this.game.add.tween(this._enemySprite).to({ alpha: 0 }, 2000, "Linear", true, 0, -1, true);
				var tween = this.game.add.tween(this._enemySprite.position).to({ x: pos.x - 100, y: pos.y + 300 }, 2000, "Linear", true, 0, -1, true);
			}
			this._enabled = false;
			Game.player.Death();
		}
		/**
		 * Destroys the Enemy Drone completely
		 */
		private DestroyThis()
		{
			this._enemySprite.kill();
			this.destroy();
		}
	}
}