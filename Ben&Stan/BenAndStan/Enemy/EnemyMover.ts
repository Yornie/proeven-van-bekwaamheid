﻿module BenAndStan
{
	/**
	 * The EnemyMover class is an object class which the player must avoid
	 */
	export class EnemyMover extends Phaser.Sprite
	{
		private _enemyMover: Phaser.Sprite;
		private _currentVelocity: number;
		private _maxVelocity: number;
		private _minVelocity: number;
		private _spawnPresetOnEnter: boolean = false;
		private _enabled: boolean = true;

		/**
		 * Starts and sets the starting values of the Seagull
		 * @param x starting X position
		 * @param y starting Y position
		 */
		public Start(x: number, y: number): void
		{
			this._enemyMover = this.game.add.sprite(x, y, 'seagull_idle');
			this.game.physics.arcade.enable(this._enemyMover);
			this._enemyMover.visible = true;

			this._enemyMover.anchor.set(0.5, 0.5);
			this._enemyMover.scale.setTo(0.75, 0.75);

			this._enemyMover.animations.add('idle', Phaser.ArrayUtils.numberArray(0, 15));
			this._enemyMover.play('idle', 30, true);

			var oldBodyValues = new Array<number>();

			oldBodyValues.push(this._enemyMover.body.width);
			oldBodyValues.push(this._enemyMover.body.height);
			this._enemyMover.body.setSize(oldBodyValues[0] / 1.5, oldBodyValues[1] / 2, 20, 20); //Changes hitbox

			this._currentVelocity = 250;
			this._maxVelocity = 250;
			this._minVelocity = -250;
		}
		/**
		 * Updates the Seagull
		 */
		public update(): void
		{
            if (this._enabled) //Checks whether enabled
			{
				if ((this._spawnPresetOnEnter) && this._enemyMover.position.x < this.game.world.centerX)
				{
					this._spawnPresetOnEnter = false;
					Game.prefabSpawner.spawnPreset(); //Spawns next preset
				}
				this._enemyMover.body.velocity.x = -150;
				this.game.physics.arcade.overlap(this._enemyMover, Game.player.GetPlayerSprite(), this.OnPlayerCollision, null, this);

				if (this._enemyMover.position.x < 0)
				{
					this.destroy();
				}
			}
		}
		/**
		 * Enables next preset spawn
		 */
		public EnableSpawnOnDestroy(): void
		{
			this._spawnPresetOnEnter = true;
		}

		/**
		 * Determines what the Seagull does on Player Collision
		 * @param sprite colliding sprite
		 */
		
		private OnPlayerCollision(sprite): void
		{
			if (Game.player.GetBlockActivated) //Checks if the player can negate
			{
				var pos: Phaser.Point = this._enemyMover.position;
				this._enemyMover.visible = false;
				this._enemyMover = this.game.add.sprite(pos.x, pos.y, 'seagull_hit'); //Changes sprite
				this._enemyMover.visible = true;
				this._enemyMover.anchor.set(0.5, 0.5);
				this._enemyMover.scale.setTo(0.75, 0.75);
				this._enemyMover.animations.add('hit', Phaser.ArrayUtils.numberArray(0, 20));
				this._enemyMover.play('hit', 30, false);
				this.game.time.events.add(Phaser.Timer.SECOND * 1.5, this.DestroyThis, this);
				var tween = this.game.add.tween(this._enemyMover).to({ alpha: 0 }, 1500, "Linear", true, 0, -1, true);
				var tween = this.game.add.tween(this._enemyMover.position).to({ x: pos.x - 100, y: pos.y + 300 }, 1500, "Linear", true, 0, -1, true);
			}
			this._enabled = false;
			Game.player.Death();
		}
		/**
		 * Destroys the Seagull completely
		 */
		private DestroyThis()
		{
			this._enemyMover.kill();
			this.destroy();
		}
	}
}