﻿module BenAndStan
{
	/**
	 * VehicleController controls the boat animations and the foam VFX
	 */
    export class VehicleController extends Phaser.Sprite
    {
        private _currentVehicle: Phaser.Sprite;
		private _line: Phaser.Sprite;
		private _boatFoamVFX: Phaser.Sprite;

		/**
		 * Starts the Vehicle Controller with given x and y values
		 * @param x starting x position
		 * @param y starting y position
		 */
        public Start(x: number, y: number): void
		{
			Game.vehicle = this;
            this._currentVehicle = this.game.add.sprite(x, y, 'stan_startup');
            this._currentVehicle.visible = true;
            this._currentVehicle.anchor.set(0.5, 0.5);
			this._currentVehicle.scale.set(1,1);

			//Adds the start animation
			this._currentVehicle.animations.add('startup', Phaser.ArrayUtils.numberArray(0, 23));
			this._currentVehicle.animations.play('startup', 30, false);
			this._currentVehicle.animations.currentAnim.onComplete.add(function () //Plays the loop animation after completion
			{
				this._currentVehicle.loadTexture('stan_selfie', 0);
				this._currentVehicle.animations.add('selfie', Phaser.ArrayUtils.numberArray(0, 54));
				this._currentVehicle.animations.play('selfie', 30, true);
			}, this);

			this._line = this.game.add.sprite(250, 50, 'blackPixel');
			this._line.scale.x = 570;
			this._line.alpha = 0.5;

			//Adds the foam VFX
			this._boatFoamVFX = this.game.add.sprite(this.game.world.centerX, 650, 'boat_foam01');
			this._boatFoamVFX.anchor.set(0.5, 0.5);
			this._boatFoamVFX.alpha = 0.5;
			this._boatFoamVFX.scale.set(1.7, 1);

			this.PlayFirstFoam();
			
            this.game.add.existing(this);
		}

		/**
		 * Plays the first Foam VFX
		 */
		private PlayFirstFoam()
		{
			this._boatFoamVFX.loadTexture('boat_foam01', 0);
			this._boatFoamVFX.animations.add('foam', Phaser.ArrayUtils.numberArray(0, 36));
			this._boatFoamVFX.animations.play('foam', 30, false);
			this._boatFoamVFX.animations.currentAnim.onComplete.add(function ()
			{
				this.PlaySecondFoam();
			}, this);
		}

		/**
		 * Plays the second Foam VFX
		 */
		private PlaySecondFoam(): void
		{
			this._boatFoamVFX.loadTexture('boat_foam02', 0);
			this._boatFoamVFX.animations.add('foam', Phaser.ArrayUtils.numberArray(0, 19));
			this._boatFoamVFX.animations.play('foam', 30, false);
			this._boatFoamVFX.animations.currentAnim.onComplete.add(function ()
			{
				this.PlayFirstFoam();
			}, this);
		}

		/**
		 * Updates the Vehicle Controller
		 */
        public update(): void
		{
			if (Game.player.GetPlayerSprite().alive) //Checks whether the player is alive
			{
				this._line.position = Game.player.GetPlayerBody.position;
				this._line.scale.x = Phaser.Point.distance(this._currentVehicle.position, Game.player.GetPlayerBody.position).valueOf() - 47;
				var angle = this.game.physics.arcade.angleBetween(this._line, this._currentVehicle.position);
				this._line.angle = angle * 57;
			} else if (this._line.exists)
			{
				this._line.kill();
			}
		}

		/**
		 * Gets the Vehicle Sprite
		 */
		public VehicleSprite(): Phaser.Sprite
		{
			return this._currentVehicle;
		}

		/**
		 * Gets the Vehicle Foam VFX
		 */
		public VehicleVFX(): Phaser.Sprite
		{
			return this._boatFoamVFX;
		}
		/**
		 * Gets the Vehicle Line
		 */
		public VehicleLine(): Phaser.Sprite
		{
			return this._line;
		}
    }
}