﻿module BenAndStan
{
	/**
	 * The Player Controller controls the player input on the player, controls the player movement and VFX
	 */
    export class PlayerController extends Phaser.Sprite
	{
		// Player variables
		private _player: Phaser.Sprite;
		private _fallVFX: Phaser.Sprite;
		private _splashVFX: Phaser.Sprite;
		private _playerVelocity: number;
		private _upReleased: boolean;
		private _playerBody: Phaser.Physics.Arcade.Body;

		private _oldBodyValues: number[];

		private _blockActivated: boolean = false;
		private _blockAmount: number = 0;
		private _enabled: boolean = true;

		/**
		 * Creates the player inside Player Controller.
		 */
		public CreatePlayer(): void
		{
			this._player = this.game.add.sprite(90, 0, 'player');
			
			/**
			*  Adds the dive VFX
			*/
			this._fallVFX = this.game.add.sprite(this._player.position.x, this._player.position.y, 'fallVFX');
			this._fallVFX.anchor.set(0.5, 0.5);
			this._fallVFX.rotation = 1;

			/**
			*  Enables physics on the player
			*/
            this.game.physics.arcade.enable(this._player, false);

			this._playerBody = this._player.body;			
			this._playerBody.gravity.y = -150;
			this._playerBody.collideWorldBounds = true;
			
			/**
			*  Adds animations to the player and the dive vfx
			*/
			this._player.animations.add('idle', Phaser.ArrayUtils.numberArray(0, 30));
			this._fallVFX.animations.add('fall', Phaser.ArrayUtils.numberArray(0, 18));

			this._playerVelocity = 0;
			this._upReleased = true;

			Game.player = this;
			
			this.LaunchPlayer();
			
			/**
			*  Idle animation
			*/
			this._player.animations.play('idle', 30, true);
			this._player.anchor.set(0.5, 0.75);
			this._player.scale.setTo(0.6, 0.6);

			this._oldBodyValues = new Array<number>();
			this._oldBodyValues.push(this._playerBody.width);
			this._oldBodyValues.push(this._playerBody.height);
			
			/**
			*  Changes the shape of the collision area
			*/
            this._playerBody.setSize(this._oldBodyValues[0] / 3.7, this._oldBodyValues[1] / 4, 150, 320);
			this.game.add.existing(this);
        }

		/**
		 * Updates the PlayerController every frame
		 */
		public update(): void
		{
			/**
			*  Sets the spacebar input
			*/
			if (this._enabled)
			{
				var cursors = this.game.input.keyboard.addKey(32);
				if (this._fallVFX.visible)
				{
					this._fallVFX.position = this._player.position;
				}
				/**
				*  Checks if the spacebar isn't pressed
				*/
				if (cursors.isUp)
				{
					if (this._fallVFX.visible)
					{
						this._fallVFX.visible = false;
					}
					if (this._player.position.y > 250)
					{
						this._playerBody.gravity.y = -250;
					} else
					{
						this._playerBody.gravity.y = 0;
					}
				}
				/**
				*	Checks if the spacebar is pressed
				*/

				if (cursors.isDown)
				{
					if (this._fallVFX.visible == false)
					{
						this._fallVFX.visible = true;
						this._fallVFX.animations.play('fall', 30, true);
					}

					this._playerBody.gravity.y = 550;
					this._upReleased = false;

					/**
					*	If the player has upwards velocity, divide it till it is 0
					*/
					if (this._playerBody.velocity.y < 0)
					{
						this._playerBody.velocity.y /= 1.2;
					}

					if (this._playerVelocity < 210)
					{
						this._playerVelocity += 30;
					}
				}
				else if (cursors.isUp && this._upReleased == false)
				{
					this.LaunchPlayer();
				}
				/**
				*	When the player reaches the water, the player dies
				*/
				if (this._playerBody.y >= 576)
				{
					this._splashVFX = this.game.add.sprite(this._player.position.x + 20, this._player.position.y - 75, 'waterSplashVFX');
					this._splashVFX.anchor.set(0.5, 0.5);
					this._splashVFX.animations.add('splash', Phaser.ArrayUtils.numberArray(0, 24));
					this._splashVFX.play('splash', 30, false);
					var tween = this.game.add.tween(this._splashVFX.position).to({ x: this._splashVFX.position.x - 100 }, 1500, "Linear", true, 0, 0, false);

					this.Death();
				}
				else if (this._playerBody.y < 100)
				{
					this._playerBody.y = 100;
					this._playerBody.velocity.y = 0;
				}
			}
		}

		/**
		 * Launches the player in an upwards direction
		 */
		private LaunchPlayer(): void
		{
			this.game.sound.play('jump_sfx', 1, false);
			this._upReleased = true;
			this._playerBody.velocity.y = -this._playerVelocity;
			this._playerVelocity = 0;
		}

		/**
		* Gets the player sprite
		*/
		public GetPlayerSprite(): Phaser.Sprite
		{
			return this._player;
		}
		/**
		* Gets the player body
		*/
		get GetPlayerBody(): Phaser.Physics.Arcade.Body
		{
			return this._playerBody;
		}
		
		/**
		* The death function determines what happens to the player
		*/
		public Death(): void
		{
			if (this._enabled)
			{
				/**
				* Checks whether the player can negate death
				*/
				if (this._blockActivated)
				{
					this.game.sound.play('hit_sfx', 1, false);
					this._player.loadTexture('player_hit', 0);
					this._player.animations.add('player_hit', Phaser.ArrayUtils.numberArray(0, 30));
					this._player.animations.play('player_hit', 30, false);

					this._blockAmount--;
					if (this._blockAmount <= 0)
					{
						this._blockActivated = false;
					}

					this._player.animations.currentAnim.onComplete.add(function ()
					{
						if (this._blockAmount <= 0)
						{
							this._blockActivated = false;
							this._player.loadTexture('player', 0);
							this._player.animations.add('idle', Phaser.ArrayUtils.numberArray(0, 30));
							this._player.animations.play('idle', 30, true);
						} else
						{
							this._player.loadTexture('ben_hit_01', 0);
							this._player.animations.add('idle_hit', Phaser.ArrayUtils.numberArray(0, 24));
							this._player.animations.play('idle_hit', 30, true);
						}
					}, this);
				} else
				{
					this.game.sound.play('death_ambient', 1, false);
					this._playerBody.velocity.y = 0;
					this._playerBody.gravity.y = 450;
					this._enabled = false;
					this._fallVFX.kill();

					this._player.position.x = this._player.position.x + 40;
					var tween = this.game.add.tween(this._player.position).to({ x: this._player.position.x - 400 }, 4500, "Linear", true, 0, 0, false);

					this.NextDeathAnim('ben_fall_01');
				}
			}
		}
		
		/**
		* Plays the death animation in the correct order
		 * @param string string that links the sprite to the key
		*/
		private NextDeathAnim(string: string)
		{
			if (string != "ben_fall_03")
			{
				this._player.loadTexture(string, 0);
				this._player.animations.add('fall', Phaser.ArrayUtils.numberArray(0, 30));
				this._player.animations.play('fall', 30, false);
				this._player.animations.currentAnim.onComplete.add(function ()
				{
					if (string == "ben_fall_01")
					{
						this.NextDeathAnim("ben_fall_02");
					} else
					{
						this.NextDeathAnim("ben_fall_03");
					}
				}, this);

			} else
			{
				this._player.loadTexture(string, 0);
				this._player.animations.add('fall', Phaser.ArrayUtils.numberArray(0, 5));
				this._player.animations.play('fall', 15, true);

				this.game.time.events.add(Phaser.Timer.SECOND * 2.5, this.RestartGame, this);
			}
		}

		/*
		*Enables death negation of the player
		*/
		public EnableBlock(): void
		{
			this._blockActivated = true;
			this._blockAmount = 2;

			this._player.loadTexture('ben_hit_01', 0);
			this._player.animations.add('idle_hit', Phaser.ArrayUtils.numberArray(0, 24));
			this._player.animations.play('idle_hit', 30, true);
		}

		/**
		* Gets the blockActivated
		*/
		public get GetBlockActivated() : boolean
		{
			return this._blockActivated;
		}

		/**
		 * Restarts the game on Death
		 */
		private RestartGame(): void
		{
			this.game.sound.stopAll();
			this.game.state.start('Boot', true, false);
		}

		/**
		* Gets the fall VFX
		*/
		public FallVFX(): Phaser.Sprite
		{
			return this._fallVFX;
		}
	}
}