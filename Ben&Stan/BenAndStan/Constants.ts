﻿module BenAndStan
{
	/**
	 * Public static constant variables will be set here and can be accessed everywhere.
	 */
	export class Constants
	{
		//Parallax movement speed
		public static THIRDLAYER_SPEED: number = 1.2;;
		public static SECONDLAYER_SPEED: number = 1.8;
		public static FIRSTLAYER_SPEED: number = 2.3;
		public static MAINLAYER_SPEED: number = 3;
		public static FRONTLAYER_SPEED: number = 5;
		//Parallax positioning
		public static THIRDLAYER_Y: number = -50;	//Backlayer
		public static SECONDLAYER_Y: number = 100;	
		public static FIRSTLAYER_Y: number = 175;
		public static PLAYERLAYER_Y: number = 250;	//PlayerLayer 
		public static FRONTLAYER_Y: number = 330;	//Front layer

		//Game values
		public static COIN_VALUE: number = 1;
		public static PLAYER_SPEED: number = 2;
		//UI
		public static SCOREFONTSIZE: number = 35;
	}
}
