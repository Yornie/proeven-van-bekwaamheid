window.onload = () => {
    var game = new BenAndStan.Game();
};
var BenAndStan;
(function (BenAndStan) {
    /**
     * This class controls the background parallax images.
     */
    class BackGroundController {
        /**
         * Constructs the Background Controller
         * @param game set the _game variable according to the given parameter
         */
        constructor(game) {
            this._game = game;
        }
        /**
         * Creates the Backgrounds Controller
         * Creates group layers
         * Adding all the parallax key strings to the corresponding arrays
         */
        create() {
            //Creating the layers for the parallax and adding it to 1 main group
            //The groups are orderd from the back to the front layer
            this._mainGroup = this._game.add.group();
            this._thirdLayer = this._game.add.group();
            this._secondLayer = this._game.add.group();
            this._firstLayer = this._game.add.group();
            this._playerLayer = this._game.add.group();
            this._frontLayer = this._game.add.group();
            //Setting layer Y position
            this._secondLayer.position.y = this._game.world.centerY;
            this._thirdLayer.position.y = this._game.world.centerY;
            this._firstLayer.position.y = this._game.world.centerY;
            this._frontLayer.position.y = this._game.world.centerY;
            this._mainGroup.pivot.set(0.5, 0.5);
            //setting layer X start positions
            this._secondLayer.position.x = this._game.world.centerX;
            this._thirdLayer.position.x = this._game.world.centerX;
            this._firstLayer.position.x = this._game.world.centerX;
            this._frontLayer.position.x = this._game.world.centerX;
            //Creating the arrays
            //front layer strings
            this._frontStrings = new Array();
            this._frontStrings.push('Object0100');
            this._frontStrings.push('Object0200');
            this._frontStrings.push('Object0300');
            this._frontStrings.push('Object0400');
            //first layer strings
            this._firstStrings = new Array();
            this._firstStrings.push('Object0101');
            this._firstStrings.push('Object0201');
            this._firstStrings.push('Object0301');
            this._firstStrings.push('Object0401');
            //second layer strings
            this._secondStrings = new Array();
            this._secondStrings.push('Object0102');
            this._secondStrings.push('Object0202');
            this._secondStrings.push('Object0302');
            //third layer strings
            this._thirdStrings = new Array();
            this._thirdStrings.push('mountain01');
            this._thirdStrings.push('mountain02');
            //this._thirdStrings.push('mountain04');
            this._thirdStrings.push('mountain05');
            //this._thirdStrings.push('mountain06');
            //adding the player sprite to the playerlayer
            this._playerLayer.add(BenAndStan.Game.player.GetPlayerSprite());
            this._playerLayer.add(BenAndStan.Game.player.FallVFX());
            this._playerLayer.add(BenAndStan.Game.vehicle.VehicleSprite());
            this._playerLayer.add(BenAndStan.Game.vehicle.VehicleVFX());
            this._playerLayer.add(BenAndStan.Game.vehicle.VehicleLine());
            //Creating the first objects for the parallax
            this._background = this._mainGroup.create(this._game.world.centerX, this._game.world.centerY, 'skyMain');
            this._frontPara = this._frontLayer.create(this._game.world.bounds.right + this._game.world.centerX, BenAndStan.Constants.FRONTLAYER_Y, this._frontStrings[Math.floor(Math.random() * this._frontStrings.length)]);
            this._firstPara = this._firstLayer.create(this._game.world.bounds.right + this._game.world.centerX, BenAndStan.Constants.FIRSTLAYER_Y, this._firstStrings[Math.floor(Math.random() * this._firstStrings.length)]);
            this._secondPara = this._secondLayer.create(this._game.world.bounds.right + this._game.world.centerX, BenAndStan.Constants.SECONDLAYER_Y, this._secondStrings[Math.floor(Math.random() * this._secondLayer.length)]);
            this._thirdPara = this._thirdLayer.create(this._game.world.bounds.right + this._game.world.centerX, BenAndStan.Constants.THIRDLAYER_Y, this._thirdStrings[Math.floor(Math.random() * this._thirdStrings.length)]);
            this._startScreen = this._firstLayer.create(this._game.world.bounds.left + this._game.world.centerX / 9.5, -100, 'mainStart');
            this._startBackScreen = this._secondLayer.create(this._game.world.left * 7, -15, 'bgHouse');
            //setting pivots
            this._startScreen.pivot.set(0, 1);
            this._frontPara.pivot.set(0, 1);
            this._firstPara.pivot.set(0, 1);
            this._secondPara.pivot.set(0, 1);
            this._thirdPara.pivot.set(0, 1);
            //setting anchors
            this._frontPara.anchor.set(0.5, 0.5);
            this._firstPara.anchor.set(0.5, 0.5);
            this._secondPara.anchor.set(0.5, 0.5);
            this._thirdPara.anchor.set(0.5, 0.5);
            this._startScreen.anchor.set(0.5, 0.5);
            this._background.anchor.set(0.5, 0.5);
        }
        /**
         * Move layers + checks if the location of the parallax is past x-value and sets them back and gets a new key from the array
         */
        MoveAllLayers() {
            this._frontPara.position.x -= BenAndStan.Constants.FRONTLAYER_SPEED;
            this._firstPara.position.x -= BenAndStan.Constants.FIRSTLAYER_SPEED;
            this._secondPara.position.x -= BenAndStan.Constants.SECONDLAYER_SPEED;
            this._thirdPara.position.x -= BenAndStan.Constants.THIRDLAYER_SPEED;
            //Start
            if (this._startScreen.position.x >= this._game.world.bounds.left + this._startScreen.width * 2.5 && this._startBackScreen.position.x >= this._game.world.bounds.left + this._startBackScreen.width * 2.5) {
                this._startScreen.destroy();
                this._startBackScreen.destroy();
            }
            else {
                this._startScreen.position.x -= BenAndStan.Constants.FIRSTLAYER_SPEED;
                this._startBackScreen.position.x -= BenAndStan.Constants.SECONDLAYER_SPEED;
            }
            //FrontLayer
            if (this._frontPara.position.x <= this._game.world.bounds.left - this._frontPara.width * 2.5) {
                this._frontPara.loadTexture(this._frontStrings[Math.floor(Math.random() * this._frontStrings.length)]);
                this._frontPara.pivot.set(0, 1);
                this._frontPara.position.x = this._game.world.bounds.right + this._frontPara.width;
            }
            //firstLayer
            if (this._firstPara.position.x <= this._game.world.bounds.left - this._firstPara.width * 6) {
                this._firstPara.loadTexture(this._firstStrings[Math.floor(Math.random() * this._firstStrings.length)]);
                this._firstPara.pivot.set(0, 1);
                this._firstPara.position.x = this._game.world.bounds.right + this._firstPara.width;
            }
            //Secondlayer
            if (this._secondPara.position.x <= this._game.world.bounds.left - this._secondPara.width * 15) {
                this._secondPara.loadTexture(this._secondStrings[Math.floor(Math.random() * this._secondStrings.length)]);
                this._secondPara.pivot.set(0, 1);
                this._secondPara.position.x = this._game.world.bounds.right + this._secondPara.width;
            }
            //ThridLayer
            if (this._thirdPara.position.x <= this._game.world.bounds.left - this._thirdPara.width * 2.5) {
                this._thirdPara.loadTexture(this._thirdStrings[Math.floor(Math.random() * this._thirdStrings.length)]);
                this._thirdPara.pivot.set(0, 1);
                this._thirdPara.position.x = this._game.world.bounds.right + this._game.world.centerX;
            }
        }
    }
    BenAndStan.BackGroundController = BackGroundController;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * BlockPowerUp is a class which is an powerup object which gives the player a boost when picked up
     */
    class BlockPowerUp extends Phaser.Sprite {
        constructor() {
            super(...arguments);
            this._enabled = true;
        }
        /**
         * The Start function which sets the values of the PowerUp
         * @param x starting X position
         * @param y starting Y position
         */
        Start(x, y) {
            this._blockSprite = this.game.add.sprite(x, y, 'hitter');
            this.game.physics.arcade.enable(this._blockSprite);
            this._blockSprite.visible = true;
            this._blockSprite.scale.setTo(0.75, 0.75);
            var oldBodyValues = new Array();
            this._currentSpeed = -150;
            this._maxVelocity = 250;
            this._minVelocity = -250;
            this.game.add.existing(this);
        }
        /**
         * Updates the powerup
         */
        update() {
            /**
            *	Checks whether it's enabled
            */
            if (this._enabled) {
                this._blockSprite.body.velocity.x = -150;
                if (this._blockSprite.position.x + 50 < 0) {
                    this._blockSprite.position.x = this.game.world.bounds.right + 50;
                    this._blockSprite.position.y = 50 + Math.random() * (this.game.world.height - 50);
                }
                this.Bounce();
                this.game.physics.arcade.overlap(this._blockSprite, BenAndStan.Game.player.GetPlayerSprite(), this.Activate, null, this);
            }
        }
        /**
         * Lets the powerup bounce up and down
         */
        Bounce() {
            var game = this.game;
            if (this._blockSprite.position.y > game.world.centerY - 200) {
                this._currentSpeed -= 3.5;
            }
            if (this._blockSprite.position.y < game.world.centerY + 200) {
                this._currentSpeed += 3.5;
            }
            if (this._currentSpeed >= this._maxVelocity) {
                this._currentSpeed = this._maxVelocity;
            }
            else if (this._currentSpeed <= this._minVelocity) {
                this._currentSpeed = this._minVelocity;
            }
            this._blockSprite.body.velocity.y = this._currentSpeed;
        }
        /**
         * Determines what the powerup does and spawns particles
         * @param sprite sprite location
         */
        Activate(sprite) {
            this.game.sound.play('powerup_sfx', 1, false);
            this._enabled = false;
            var pos = this._blockSprite.position;
            this._pickupVFX = this.game.add.sprite(pos.x, pos.y, 'boost01VFX');
            this._pickupVFX.anchor.set(0.5, 0.5);
            this._pickupVFX.scale.set(0.75, 0.75);
            this._pickupVFX.animations.add('pickup', Phaser.ArrayUtils.numberArray(0, 16));
            this._pickupVFX.play('pickup', 30, false);
            var tween = this.game.add.tween(this._blockSprite.scale).to({ x: 0, y: 0 }, 500, "Linear", true, 0, -1, true);
            var tween = this.game.add.tween(this._blockSprite).to({ alpha: 0 }, 1000, "Linear", true, 0, -1, true);
            var tween = this.game.add.tween(this._pickupVFX).to({ alpha: 0 }, 1600, "Linear", true, 0, -1, true);
            var tween = this.game.add.tween(this._pickupVFX.position).to({ x: pos.x - 150 }, 2000, "Linear", true, 0, -1, true);
            this.game.time.events.add(Phaser.Timer.SECOND * 2, this.DestroyThis, this);
            BenAndStan.Game.player.EnableBlock();
        }
        /**
         * Destroys the powerup completely
         */
        DestroyThis() {
            this._pickupVFX.kill();
            this._blockSprite.kill();
            this.destroy();
        }
    }
    BenAndStan.BlockPowerUp = BlockPowerUp;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * The basic Coin class which is an pickupable object that adds score when picked up
     */
    class Coin extends Phaser.Sprite {
        constructor() {
            super(...arguments);
            this._canAttract = false;
            this._random = false;
            this._spawnPresetOnEnter = false;
            this._enabled = true;
        }
        /**
         * Sets the values of the Coin
         * @param x starting X position
         * @param y starting Y position
         */
        Start(x, y, random) {
            this._random = random;
            this._playerSprite = BenAndStan.Game.player.GetPlayerSprite();
            this._coinSprite = this.game.add.sprite(x, y, 'coin');
            this.game.physics.arcade.enable(this._coinSprite);
            this._coinSprite.visible = true;
            this._coinSprite.anchor.set(0.5, 0.5);
            this._coinSprite.animations.add('coinspin', Phaser.ArrayUtils.numberArray(0, 16), 16);
            this._coinSprite.play('coinspin', 16, true);
            this._currentSpeed = -150;
            this.game.add.existing(this);
            BenAndStan.Game.objectPool.AddCoin(this);
        }
        /**
         * Updates the Coin
         */
        update() {
            if (this._enabled) {
                if ((this._spawnPresetOnEnter) && this._coinSprite.position.x < this.game.world.centerX) {
                    this._spawnPresetOnEnter = false;
                    BenAndStan.Game.prefabSpawner.spawnPreset();
                }
                this._coinSprite.body.velocity.x = this._currentSpeed;
                this.game.physics.arcade.overlap(this._coinSprite, this._playerSprite, this.AddScore, null, this);
                if (this._coinSprite.position.x + 50 < 0) {
                    this._coinSprite.kill();
                    this.destroy();
                }
                if (this._canAttract) {
                    this.Attract();
                }
                else if (this._coinSprite.body.velocity.y != 0) {
                    this._coinSprite.body.velocity.y = 0;
                }
            }
        }
        /**
         * Adds score on pickup
         */
        AddScore() {
            this._enabled = false;
            this._currentSpeed = -150;
            var pos = this._coinSprite.position;
            /**
            * Spawns the pickup VFX
            */
            this._pickupVFX = this.game.add.sprite(pos.x, pos.y, 'coinPVFX');
            this._pickupVFX.anchor.set(0.5, 0.5);
            this._pickupVFX.scale.set(1.2, 1.2);
            this._pickupVFX.animations.add('pickup', Phaser.ArrayUtils.numberArray(0, 28));
            this._pickupVFX.play('pickup', 30, false);
            /**
             * Fades the coin and moves the VFX
             */
            var tween = this.game.add.tween(this._coinSprite.scale).to({ x: 0, y: 0 }, 500, "Linear", true, 0, -1, true);
            var tween = this.game.add.tween(this._coinSprite).to({ alpha: 0 }, 1000, "Linear", true, 0, -1, true);
            var tween = this.game.add.tween(this._pickupVFX.position).to({ x: pos.x - 150 }, 2000, "Linear", true, 0, -1, true);
            this.game.time.events.add(Phaser.Timer.SECOND * 2, this.DestroyThis, this);
            this.game.sound.play('coin_sfx', 0.1, false);
            BenAndStan.Game.goldController.AddGold(BenAndStan.Constants.COIN_VALUE);
        }
        /**
         * Destroys the Coin completely
         */
        DestroyThis() {
            this._pickupVFX.kill();
            this._pickupVFX.destroy();
            this._coinSprite.kill();
            this.destroy();
        }
        /**
         * Toggles the attract boolean
         * @param bool sets the _canAttract boolean according to the given parameter boolean
         */
        ToggleAttract(bool) {
            this._canAttract = bool;
        }
        /**
         * Gets the Attract boolelan
         */
        AttractEnabled() {
            return this._canAttract;
        }
        /**
         * Enables the next preset spawn
         */
        EnableSpawnOnDestroy() {
            this._spawnPresetOnEnter = true;
        }
        /**
         * Attracts to coin towards the player if in a certain range
         */
        Attract() {
            var dist;
            dist = this._coinSprite.position.distance(this._playerSprite.position);
            if (this._playerSprite.position.x < this._coinSprite.position.x && dist < 250) {
                this._currentSpeed = -200;
                if (this._playerSprite.position.y - 5 > this._coinSprite.position.y) {
                    this._coinSprite.body.velocity.y = 100;
                }
                else if (this._playerSprite.position.y + 5 < this._coinSprite.position.y) {
                    this._coinSprite.body.velocity.y = -100;
                }
                else {
                    this._coinSprite.body.velocity.y = 0;
                }
            }
            else {
                this._coinSprite.body.velocity.y = 0;
                this._currentSpeed = -150;
            }
        }
    }
    BenAndStan.Coin = Coin;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * The CoinMagnet class creates an object that you can pickup to draw nearby coins towards the player
     */
    class CoinMagnet extends Phaser.Sprite {
        constructor() {
            super(...arguments);
            this._enabled = true;
        }
        /**
         * Sets the starting values of the coin magnet
         * @param x starting x position
         * @param y starting y position
         * @param objectPool sets the _objectPool variable according to the given parameter
         */
        Start(x, y, objectPool) {
            this._objectPool = objectPool;
            this._magnetSprite = this.game.add.sprite(x, y, 'coinMagnet');
            this.game.physics.arcade.enable(this._magnetSprite);
            this._magnetSprite.visible = true;
            this._magnetSprite.scale.setTo(0.75, 0.75);
            var oldBodyValues = new Array();
            this._currentSpeed = -150;
            this._maxVelocity = 250;
            this._minVelocity = -250;
            this.game.add.existing(this);
        }
        /**
         * Updates the coin
         */
        update() {
            if (this._enabled) {
                this._magnetSprite.body.velocity.x = -150;
                this.Bounce();
                this.game.physics.arcade.overlap(this._magnetSprite, BenAndStan.Game.player.GetPlayerSprite(), this.Activate, null, this);
            }
            else {
                this._objectPool.ToggleAttract(true);
                this._pickupVFX.position = BenAndStan.Game.player.GetPlayerBody.position;
            }
        }
        /**
         * Lets the magnet bounce up and down
         */
        Bounce() {
            var game = this.game;
            if (this._magnetSprite.position.y > game.world.centerY - 200) {
                this._currentSpeed -= 7.5;
            }
            if (this._magnetSprite.position.y < game.world.centerY + 200) {
                this._currentSpeed += 7.5;
            }
            if (this._currentSpeed >= this._maxVelocity) {
                this._currentSpeed = this._maxVelocity;
            }
            else if (this._currentSpeed <= this._minVelocity) {
                this._currentSpeed = this._minVelocity;
            }
            this._magnetSprite.body.velocity.y = this._currentSpeed;
        }
        /**
         * Determines what it does on activation
         * @param sprite sprite location
         */
        Activate(sprite) {
            this.game.sound.play('powerup_sfx', 1, false);
            var pos = this._magnetSprite.position;
            /**
            *	Spawns the pickup VFX
            */
            this._pickupVFX = this.game.add.sprite(pos.x, pos.y, 'coinMagVFX');
            this._pickupVFX.anchor.set(0.35, 0.35);
            this._pickupVFX.scale.set(0.8, 0.8);
            this._pickupVFX.animations.add('pickup', Phaser.ArrayUtils.numberArray(0, 25));
            this._pickupVFX.play('pickup', 30, true);
            this.game.time.events.add(Phaser.Timer.SECOND * 10, this.DisableMagnet, this);
            var tween = this.game.add.tween(this._magnetSprite.scale).to({ x: 0, y: 0 }, 500, "Linear", true, 0, 0, false);
            var tween = this.game.add.tween(this._magnetSprite).to({ alpha: 0 }, 1000, "Linear", true, 0, 0, false);
            var tween = this.game.add.tween(this._pickupVFX).to({ alpha: 0 }, 1000, "Linear", true, 9000, 0, false);
            this._enabled = false;
        }
        /**
         * Disables the magnet
         */
        DisableMagnet() {
            this._objectPool.ToggleAttract(false);
            this.destroy();
        }
    }
    BenAndStan.CoinMagnet = CoinMagnet;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * Public static constant variables will be set here and can be accessed everywhere.
     */
    class Constants {
        ;
    }
    //Parallax movement speed
    Constants.THIRDLAYER_SPEED = 1.2;
    Constants.SECONDLAYER_SPEED = 1.8;
    Constants.FIRSTLAYER_SPEED = 2.3;
    Constants.MAINLAYER_SPEED = 3;
    Constants.FRONTLAYER_SPEED = 5;
    //Parallax positioning
    Constants.THIRDLAYER_Y = -50; //Backlayer
    Constants.SECONDLAYER_Y = 100;
    Constants.FIRSTLAYER_Y = 175;
    Constants.PLAYERLAYER_Y = 250; //PlayerLayer 
    Constants.FRONTLAYER_Y = 330; //Front layer
    //Game values
    Constants.COIN_VALUE = 1;
    Constants.PLAYER_SPEED = 2;
    //UI
    Constants.SCOREFONTSIZE = 35;
    BenAndStan.Constants = Constants;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * The Goldcontroller controls the display of the gold text
     */
    class GoldController {
        /**
         * Constructs the Gold Controller
         * @param game sets the _game variable according to the given parameter
         */
        constructor(game) {
            this._currentGold = 0;
            this._game = game;
            BenAndStan.Game.goldController = this;
        }
        /**
         * Creates the Gold Text which is displayed in the screen
         */
        create() {
            this._goldText = this._game.add.text(this._game.world.bounds.right - 375, this._game.world.bounds.top + 75, 'Gold : 0', { fontSize: BenAndStan.Constants.SCOREFONTSIZE, fill: '#e9e6d9', font: 'BigJohn' });
        }
        /**
         * Adds gold to the Gold Total
         * @param amount adds an 'amount' to the _currentGold variable
         */
        AddGold(amount) {
            this._currentGold += amount;
            this._goldText.text = "Gold : " + this._currentGold;
        }
    }
    BenAndStan.GoldController = GoldController;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * The ParticleController class manages particles.
     */
    class ParticleController {
        /**
         * Constructs the Particle Controller
         * @param _game sets the game variable according to the given parameter
         */
        constructor(_game) {
            this.game = _game;
            BenAndStan.Game.particleController = this;
        }
        /**
         * Spawns particles with all its given values
         * @param position position of the particles
         * @param key string links to a sprite
         * @param amount number of particles to spawn
         * @param lifespan duration of the particles
         * @param size size of the particles
         * @param xSpeed speed of the particles on the X axis
         * @param xOffSet amount of distance the particles spawn from the original position on the X axis
         * @param yOffSet amoutn of distance the particles spawn from the original position on the Y axis
         */
        SpawnParticles(position, key, amount, lifespan, size, xSpeed, xOffSet, yOffSet) {
            if (xSpeed == undefined || xSpeed == null) {
                xSpeed = 0;
            }
            if (xOffSet == undefined || xOffSet == null) {
                xOffSet = 0;
            }
            if (yOffSet == undefined || yOffSet == null) {
                yOffSet = 0;
            }
            if (size == undefined || size == null) {
                size = new Phaser.Point(1, 1);
            }
            this.particleEmitter = this.game.add.emitter(0, 0, 10); //Adds emitter to the game
            this.particleEmitter.makeParticles(key); //Sets the particle sprite
            this.particleEmitter.gravity = 200; //Sets the particle gravity
            this.particleEmitter.particleBringToTop = true; //Sets the particle to the top layer
            this.particleEmitter.scale.set(size.x, size.y); // Scales the particles
            this.particleEmitter.position.x = position.x + xOffSet; //Sets the X position offset
            this.particleEmitter.position.y = position.y + yOffSet; //Sets the Y position offset
            var tween = this.game.add.tween(this.particleEmitter).to({ alpha: 0 }, 1000, "Linear", true, 0, -1, true); //Fades the particles away
            var tween = this.game.add.tween(this.particleEmitter.position).to({ x: position.x - xSpeed }, 1000, "Linear", true, 0, -1, true); //Sets speed for the particles
            this.particleEmitter.start(true, lifespan, null, amount); //Starts the emitter with all the given particle values above
        }
    }
    BenAndStan.ParticleController = ParticleController;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * The Score class mainly displays a distance meter where you can see how far you have traveled (flown)
     */
    class Score {
        /**
         * Constructs the Score class
         * @param _game sets the game varibable according to the given parameter
         */
        constructor(_game) {
            this._score = 0;
            this.game = _game;
            BenAndStan.Game.score = this;
        }
        /**
         * Creates the score text which is displayed in screen
         */
        Create() {
            this.scoreText = this.game.add.text(this.game.world.bounds.right - 375, this.game.world.bounds.top + 25, 'Meters : ' + BenAndStan.Game.score.ScoreMeters, { fontSize: BenAndStan.Constants.SCOREFONTSIZE, fill: '#e9e6d9', font: 'BigJohn' });
        }
        /**
         * Adds an amount of score to the total score
         * @param amount adds an 'amount' to the _score variable
         */
        AddScore(amount) {
            this._score += amount;
            this.scoreText.text = "Meters : " + this._score.toFixed(0);
        }
        /**
         * Gets the score meter
         */
        ScoreMeters() {
            return this._score;
        }
    }
    BenAndStan.Score = Score;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * The EnemyDrone class is an object class which the player must dodge
     */
    class EnemyDrone extends Phaser.Sprite {
        constructor() {
            super(...arguments);
            this._spawnPresetOnEnter = false;
            this._enabled = true;
        }
        /**
         * Starts the EnemyDrone and sets it's values
         * @param x starting x position
         * @param y starting y position
         */
        Start(x, y) {
            this._enemySprite = this.game.add.sprite(x, y, 'drone_idle');
            this.game.physics.arcade.enable(this._enemySprite);
            this._enemySprite.visible = true;
            this._enemySprite.anchor.set(0.5, 0.5);
            this._enemySprite.scale.setTo(0.75, 0.75);
            this._enemySprite.animations.add('idle', Phaser.ArrayUtils.numberArray(0, 20));
            this._enemySprite.play('idle', 30, true);
            var oldBodyValues = new Array();
            oldBodyValues.push(this._enemySprite.body.width);
            oldBodyValues.push(this._enemySprite.body.height);
            this._enemySprite.body.setSize(oldBodyValues[0] / 1.5, oldBodyValues[1], 20, 0); //Changes it's hitbox
            this._centerY = y;
            this._currentVelocity = 250;
            this._maxVelocity = 250;
            this._minVelocity = -250;
            this.game.add.existing(this);
        }
        /**
         * Updates the Enemy Drone
         */
        update() {
            if (this._enabled) {
                if ((this._spawnPresetOnEnter) && this._enemySprite.position.x < this.game.world.centerX) {
                    this._spawnPresetOnEnter = false;
                    BenAndStan.Game.prefabSpawner.spawnPreset();
                }
                this._enemySprite.body.velocity.x = -150;
                this.Bounce();
                this.game.physics.arcade.overlap(this._enemySprite, BenAndStan.Game.player.GetPlayerSprite(), this.OnPlayerCollision, null, this);
                if (this._enemySprite.position.x < 0) {
                    this.destroy();
                }
            }
        }
        /**
         * Lets the Enemy Drone bounce up and down
         */
        Bounce() {
            if (this._enemySprite.position.y > this._centerY) {
                this._currentVelocity -= 7.5;
            }
            if (this._enemySprite.position.y < this._centerY) {
                this._currentVelocity += 7.5;
            }
            if (this._currentVelocity >= this._maxVelocity) {
                this._currentVelocity = this._maxVelocity;
            }
            else if (this._currentVelocity <= this._minVelocity) {
                this._currentVelocity = this._minVelocity;
            }
            this._enemySprite.body.velocity.y = this._currentVelocity;
        }
        /**
         * Enables next preset spawn
         */
        EnableSpawnOnDestroy() {
            this._spawnPresetOnEnter = true;
        }
        /**
         * Determines what it does on Collision with the Player
         * @param sprite sprite location
         */
        OnPlayerCollision(sprite) {
            if (BenAndStan.Game.player.GetBlockActivated) {
                var pos = this._enemySprite.position;
                this._enemySprite.visible = false;
                this._enemySprite = this.game.add.sprite(pos.x, pos.y, 'drone_hit'); //Changes sprite
                this._enemySprite.visible = true;
                this._enemySprite.anchor.set(0.5, 0.5);
                this._enemySprite.scale.setTo(0.75, 0.75);
                this._enemySprite.animations.add('hit', Phaser.ArrayUtils.numberArray(0, 20));
                this._enemySprite.play('hit', 30, false);
                this.game.time.events.add(Phaser.Timer.SECOND * 2, this.DestroyThis, this);
                var tween = this.game.add.tween(this._enemySprite).to({ alpha: 0 }, 2000, "Linear", true, 0, -1, true);
                var tween = this.game.add.tween(this._enemySprite.position).to({ x: pos.x - 100, y: pos.y + 300 }, 2000, "Linear", true, 0, -1, true);
            }
            this._enabled = false;
            BenAndStan.Game.player.Death();
        }
        /**
         * Destroys the Enemy Drone completely
         */
        DestroyThis() {
            this._enemySprite.kill();
            this.destroy();
        }
    }
    BenAndStan.EnemyDrone = EnemyDrone;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * The EnemyMover class is an object class which the player must avoid
     */
    class EnemyMover extends Phaser.Sprite {
        constructor() {
            super(...arguments);
            this._spawnPresetOnEnter = false;
            this._enabled = true;
        }
        /**
         * Starts and sets the starting values of the Seagull
         * @param x starting X position
         * @param y starting Y position
         */
        Start(x, y) {
            this._enemyMover = this.game.add.sprite(x, y, 'seagull_idle');
            this.game.physics.arcade.enable(this._enemyMover);
            this._enemyMover.visible = true;
            this._enemyMover.anchor.set(0.5, 0.5);
            this._enemyMover.scale.setTo(0.75, 0.75);
            this._enemyMover.animations.add('idle', Phaser.ArrayUtils.numberArray(0, 15));
            this._enemyMover.play('idle', 30, true);
            var oldBodyValues = new Array();
            oldBodyValues.push(this._enemyMover.body.width);
            oldBodyValues.push(this._enemyMover.body.height);
            this._enemyMover.body.setSize(oldBodyValues[0] / 1.5, oldBodyValues[1] / 2, 20, 20); //Changes hitbox
            this._currentVelocity = 250;
            this._maxVelocity = 250;
            this._minVelocity = -250;
        }
        /**
         * Updates the Seagull
         */
        update() {
            if (this._enabled) {
                if ((this._spawnPresetOnEnter) && this._enemyMover.position.x < this.game.world.centerX) {
                    this._spawnPresetOnEnter = false;
                    BenAndStan.Game.prefabSpawner.spawnPreset(); //Spawns next preset
                }
                this._enemyMover.body.velocity.x = -150;
                this.game.physics.arcade.overlap(this._enemyMover, BenAndStan.Game.player.GetPlayerSprite(), this.OnPlayerCollision, null, this);
                if (this._enemyMover.position.x < 0) {
                    this.destroy();
                }
            }
        }
        /**
         * Enables next preset spawn
         */
        EnableSpawnOnDestroy() {
            this._spawnPresetOnEnter = true;
        }
        /**
         * Determines what the Seagull does on Player Collision
         * @param sprite colliding sprite
         */
        OnPlayerCollision(sprite) {
            if (BenAndStan.Game.player.GetBlockActivated) {
                var pos = this._enemyMover.position;
                this._enemyMover.visible = false;
                this._enemyMover = this.game.add.sprite(pos.x, pos.y, 'seagull_hit'); //Changes sprite
                this._enemyMover.visible = true;
                this._enemyMover.anchor.set(0.5, 0.5);
                this._enemyMover.scale.setTo(0.75, 0.75);
                this._enemyMover.animations.add('hit', Phaser.ArrayUtils.numberArray(0, 20));
                this._enemyMover.play('hit', 30, false);
                this.game.time.events.add(Phaser.Timer.SECOND * 1.5, this.DestroyThis, this);
                var tween = this.game.add.tween(this._enemyMover).to({ alpha: 0 }, 1500, "Linear", true, 0, -1, true);
                var tween = this.game.add.tween(this._enemyMover.position).to({ x: pos.x - 100, y: pos.y + 300 }, 1500, "Linear", true, 0, -1, true);
            }
            this._enabled = false;
            BenAndStan.Game.player.Death();
        }
        /**
         * Destroys the Seagull completely
         */
        DestroyThis() {
            this._enemyMover.kill();
            this.destroy();
        }
    }
    BenAndStan.EnemyMover = EnemyMover;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * This class creates the game engine throught HTML5 in the browser
     */
    class Game extends Phaser.Game {
        /**
         * Constructs the initial costructor of the gamne engine
         */
        constructor() {
            super(1280, 720, Phaser.AUTO, 'content');
            Game.game = this;
            //Adds states to the game
            this.state.add("Boot", BenAndStan.Boot, false);
            this.state.add("Preload", BenAndStan.Preload, false);
            this.state.add("PrepMenu", BenAndStan.PrepMenu, false);
            this.state.add("GameState", BenAndStan.GameState, false);
            this.state.start("Boot");
        }
    }
    BenAndStan.Game = Game;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    class ObjectPool {
        /**
         * Constructs the Object Pool class
         * @param game sets the _game variable according to the given parameter
         */
        constructor(game) {
            this._game = game;
            BenAndStan.Game.objectPool = this;
            this._prefabSpawner = new BenAndStan.PrefabSpawner(this._game);
            this._bgController = new BenAndStan.BackGroundController(this._game);
        }
        /**
         * Creates coin list for the pool
         */
        CreateList() {
            this._coinList = new Array();
            this._prefabSpawner.spawnPreset();
        }
        /**
         * Toggles attract for the coins in the coin list
         * @param enabled toggles the Attract variables in the coins according to the given parameter
         */
        ToggleAttract(enabled) {
            this._coinList.forEach(function (coin) {
                if (coin.AttractEnabled() != enabled) {
                    coin.ToggleAttract(enabled);
                }
            });
        }
        /**
         * Adds coin to the coin list
         * @param coin adds coin to the _coinList variable
         */
        AddCoin(coin) {
            this._coinList.push(coin);
        }
    }
    BenAndStan.ObjectPool = ObjectPool;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * The Player Controller controls the player input on the player, controls the player movement and VFX
     */
    class PlayerController extends Phaser.Sprite {
        constructor() {
            super(...arguments);
            this._blockActivated = false;
            this._blockAmount = 0;
            this._enabled = true;
        }
        /**
         * Creates the player inside Player Controller.
         */
        CreatePlayer() {
            this._player = this.game.add.sprite(90, 0, 'player');
            /**
            *  Adds the dive VFX
            */
            this._fallVFX = this.game.add.sprite(this._player.position.x, this._player.position.y, 'fallVFX');
            this._fallVFX.anchor.set(0.5, 0.5);
            this._fallVFX.rotation = 1;
            /**
            *  Enables physics on the player
            */
            this.game.physics.arcade.enable(this._player, false);
            this._playerBody = this._player.body;
            this._playerBody.gravity.y = -150;
            this._playerBody.collideWorldBounds = true;
            /**
            *  Adds animations to the player and the dive vfx
            */
            this._player.animations.add('idle', Phaser.ArrayUtils.numberArray(0, 30));
            this._fallVFX.animations.add('fall', Phaser.ArrayUtils.numberArray(0, 18));
            this._playerVelocity = 0;
            this._upReleased = true;
            BenAndStan.Game.player = this;
            this.LaunchPlayer();
            /**
            *  Idle animation
            */
            this._player.animations.play('idle', 30, true);
            this._player.anchor.set(0.5, 0.75);
            this._player.scale.setTo(0.6, 0.6);
            this._oldBodyValues = new Array();
            this._oldBodyValues.push(this._playerBody.width);
            this._oldBodyValues.push(this._playerBody.height);
            /**
            *  Changes the shape of the collision area
            */
            this._playerBody.setSize(this._oldBodyValues[0] / 3.7, this._oldBodyValues[1] / 4, 150, 320);
            this.game.add.existing(this);
        }
        /**
         * Updates the PlayerController every frame
         */
        update() {
            /**
            *  Sets the spacebar input
            */
            if (this._enabled) {
                var cursors = this.game.input.keyboard.addKey(32);
                if (this._fallVFX.visible) {
                    this._fallVFX.position = this._player.position;
                }
                /**
                *  Checks if the spacebar isn't pressed
                */
                if (cursors.isUp) {
                    if (this._fallVFX.visible) {
                        this._fallVFX.visible = false;
                    }
                    if (this._player.position.y > 250) {
                        this._playerBody.gravity.y = -250;
                    }
                    else {
                        this._playerBody.gravity.y = 0;
                    }
                }
                /**
                *	Checks if the spacebar is pressed
                */
                if (cursors.isDown) {
                    if (this._fallVFX.visible == false) {
                        this._fallVFX.visible = true;
                        this._fallVFX.animations.play('fall', 30, true);
                    }
                    this._playerBody.gravity.y = 550;
                    this._upReleased = false;
                    /**
                    *	If the player has upwards velocity, divide it till it is 0
                    */
                    if (this._playerBody.velocity.y < 0) {
                        this._playerBody.velocity.y /= 1.2;
                    }
                    if (this._playerVelocity < 210) {
                        this._playerVelocity += 30;
                    }
                }
                else if (cursors.isUp && this._upReleased == false) {
                    this.LaunchPlayer();
                }
                /**
                *	When the player reaches the water, the player dies
                */
                if (this._playerBody.y >= 576) {
                    this._splashVFX = this.game.add.sprite(this._player.position.x + 20, this._player.position.y - 75, 'waterSplashVFX');
                    this._splashVFX.anchor.set(0.5, 0.5);
                    this._splashVFX.animations.add('splash', Phaser.ArrayUtils.numberArray(0, 24));
                    this._splashVFX.play('splash', 30, false);
                    var tween = this.game.add.tween(this._splashVFX.position).to({ x: this._splashVFX.position.x - 100 }, 1500, "Linear", true, 0, 0, false);
                    this.Death();
                }
                else if (this._playerBody.y < 100) {
                    this._playerBody.y = 100;
                    this._playerBody.velocity.y = 0;
                }
            }
        }
        /**
         * Launches the player in an upwards direction
         */
        LaunchPlayer() {
            this.game.sound.play('jump_sfx', 1, false);
            this._upReleased = true;
            this._playerBody.velocity.y = -this._playerVelocity;
            this._playerVelocity = 0;
        }
        /**
        * Gets the player sprite
        */
        GetPlayerSprite() {
            return this._player;
        }
        /**
        * Gets the player body
        */
        get GetPlayerBody() {
            return this._playerBody;
        }
        /**
        * The death function determines what happens to the player
        */
        Death() {
            if (this._enabled) {
                /**
                * Checks whether the player can negate death
                */
                if (this._blockActivated) {
                    this.game.sound.play('hit_sfx', 1, false);
                    this._player.loadTexture('player_hit', 0);
                    this._player.animations.add('player_hit', Phaser.ArrayUtils.numberArray(0, 30));
                    this._player.animations.play('player_hit', 30, false);
                    this._blockAmount--;
                    if (this._blockAmount <= 0) {
                        this._blockActivated = false;
                    }
                    this._player.animations.currentAnim.onComplete.add(function () {
                        if (this._blockAmount <= 0) {
                            this._blockActivated = false;
                            this._player.loadTexture('player', 0);
                            this._player.animations.add('idle', Phaser.ArrayUtils.numberArray(0, 30));
                            this._player.animations.play('idle', 30, true);
                        }
                        else {
                            this._player.loadTexture('ben_hit_01', 0);
                            this._player.animations.add('idle_hit', Phaser.ArrayUtils.numberArray(0, 24));
                            this._player.animations.play('idle_hit', 30, true);
                        }
                    }, this);
                }
                else {
                    this.game.sound.play('death_ambient', 1, false);
                    this._playerBody.velocity.y = 0;
                    this._playerBody.gravity.y = 450;
                    this._enabled = false;
                    this._fallVFX.kill();
                    this._player.position.x = this._player.position.x + 40;
                    var tween = this.game.add.tween(this._player.position).to({ x: this._player.position.x - 400 }, 4500, "Linear", true, 0, 0, false);
                    this.NextDeathAnim('ben_fall_01');
                }
            }
        }
        /**
        * Plays the death animation in the correct order
         * @param string string that links the sprite to the key
        */
        NextDeathAnim(string) {
            if (string != "ben_fall_03") {
                this._player.loadTexture(string, 0);
                this._player.animations.add('fall', Phaser.ArrayUtils.numberArray(0, 30));
                this._player.animations.play('fall', 30, false);
                this._player.animations.currentAnim.onComplete.add(function () {
                    if (string == "ben_fall_01") {
                        this.NextDeathAnim("ben_fall_02");
                    }
                    else {
                        this.NextDeathAnim("ben_fall_03");
                    }
                }, this);
            }
            else {
                this._player.loadTexture(string, 0);
                this._player.animations.add('fall', Phaser.ArrayUtils.numberArray(0, 5));
                this._player.animations.play('fall', 15, true);
                this.game.time.events.add(Phaser.Timer.SECOND * 2.5, this.RestartGame, this);
            }
        }
        /*
        *Enables death negation of the player
        */
        EnableBlock() {
            this._blockActivated = true;
            this._blockAmount = 2;
            this._player.loadTexture('ben_hit_01', 0);
            this._player.animations.add('idle_hit', Phaser.ArrayUtils.numberArray(0, 24));
            this._player.animations.play('idle_hit', 30, true);
        }
        /**
        * Gets the blockActivated
        */
        get GetBlockActivated() {
            return this._blockActivated;
        }
        /**
         * Restarts the game on Death
         */
        RestartGame() {
            this.game.sound.stopAll();
            this.game.state.start('Boot', true, false);
        }
        /**
        * Gets the fall VFX
        */
        FallVFX() {
            return this._fallVFX;
        }
    }
    BenAndStan.PlayerController = PlayerController;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * VehicleController controls the boat animations and the foam VFX
     */
    class VehicleController extends Phaser.Sprite {
        /**
         * Starts the Vehicle Controller with given x and y values
         * @param x starting x position
         * @param y starting y position
         */
        Start(x, y) {
            BenAndStan.Game.vehicle = this;
            this._currentVehicle = this.game.add.sprite(x, y, 'stan_startup');
            this._currentVehicle.visible = true;
            this._currentVehicle.anchor.set(0.5, 0.5);
            this._currentVehicle.scale.set(1, 1);
            //Adds the start animation
            this._currentVehicle.animations.add('startup', Phaser.ArrayUtils.numberArray(0, 23));
            this._currentVehicle.animations.play('startup', 30, false);
            this._currentVehicle.animations.currentAnim.onComplete.add(function () {
                this._currentVehicle.loadTexture('stan_selfie', 0);
                this._currentVehicle.animations.add('selfie', Phaser.ArrayUtils.numberArray(0, 54));
                this._currentVehicle.animations.play('selfie', 30, true);
            }, this);
            this._line = this.game.add.sprite(250, 50, 'blackPixel');
            this._line.scale.x = 570;
            this._line.alpha = 0.5;
            //Adds the foam VFX
            this._boatFoamVFX = this.game.add.sprite(this.game.world.centerX, 650, 'boat_foam01');
            this._boatFoamVFX.anchor.set(0.5, 0.5);
            this._boatFoamVFX.alpha = 0.5;
            this._boatFoamVFX.scale.set(1.7, 1);
            this.PlayFirstFoam();
            this.game.add.existing(this);
        }
        /**
         * Plays the first Foam VFX
         */
        PlayFirstFoam() {
            this._boatFoamVFX.loadTexture('boat_foam01', 0);
            this._boatFoamVFX.animations.add('foam', Phaser.ArrayUtils.numberArray(0, 36));
            this._boatFoamVFX.animations.play('foam', 30, false);
            this._boatFoamVFX.animations.currentAnim.onComplete.add(function () {
                this.PlaySecondFoam();
            }, this);
        }
        /**
         * Plays the second Foam VFX
         */
        PlaySecondFoam() {
            this._boatFoamVFX.loadTexture('boat_foam02', 0);
            this._boatFoamVFX.animations.add('foam', Phaser.ArrayUtils.numberArray(0, 19));
            this._boatFoamVFX.animations.play('foam', 30, false);
            this._boatFoamVFX.animations.currentAnim.onComplete.add(function () {
                this.PlayFirstFoam();
            }, this);
        }
        /**
         * Updates the Vehicle Controller
         */
        update() {
            if (BenAndStan.Game.player.GetPlayerSprite().alive) {
                this._line.position = BenAndStan.Game.player.GetPlayerBody.position;
                this._line.scale.x = Phaser.Point.distance(this._currentVehicle.position, BenAndStan.Game.player.GetPlayerBody.position).valueOf() - 47;
                var angle = this.game.physics.arcade.angleBetween(this._line, this._currentVehicle.position);
                this._line.angle = angle * 57;
            }
            else if (this._line.exists) {
                this._line.kill();
            }
        }
        /**
         * Gets the Vehicle Sprite
         */
        VehicleSprite() {
            return this._currentVehicle;
        }
        /**
         * Gets the Vehicle Foam VFX
         */
        VehicleVFX() {
            return this._boatFoamVFX;
        }
        /**
         * Gets the Vehicle Line
         */
        VehicleLine() {
            return this._line;
        }
    }
    BenAndStan.VehicleController = VehicleController;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    class PrefabSpawner {
        /*
        <---- Json values ---->
        Objectlayer = ObjectLayer
        Coins : "coin"
        Seagull : "enemy_seagull"
        Drone: "enemy_drone"
        Fish: "enemy_fish"
        Coin Magnet : "powerup_coinMagnet
        */
        /**
         * Construct the prefab spawner
         * @param game sets the _game variable according to the given parameter
         */
        constructor(game) {
            this._game = game;
            BenAndStan.Game.prefabSpawner = this;
            if (!this._presetArray) {
                //Add the string from the Json file here
                this._presetArray = new Array();
                //this._presetArray.push('preset01');
                this._presetArray.push('preset02');
                this._presetArray.push('preset03');
                this._presetArray.push('preset04');
                this._presetArray.push('preset05');
                //this._presetArray.push('preset06');
                this._presetArray.push('preset07');
                this._presetArray.push('preset08');
                this._presetArray.push('preset09');
                //this._presetArray.push('preset10');
                this._presetArray.push('preset11');
                this._presetArray.push('preset12');
                this._presetArray.push('preset13');
                this._presetArray.push('preset14');
                this._presetArray.push('preset15');
                this._presetArray.push('preset16');
                this._presetArray.push('preset17');
                this._presetArray.push('preset18');
                this._presetArray.push('preset19');
                this._presetArray.push('preset20');
                this._presetArray.push('preset21');
                this._presetArray.push('preset22');
                this._presetArray.push('preset23');
                this._presetArray.push('preset24');
            }
        }
        /**
         * Spawns presets
         */
        spawnPreset() {
            //Setting local vars
            var presetArray = this._presetArray;
            var gameV = this._game;
            //Gets a random preset string
            var randomPreset = presetArray[Math.floor(Math.random() * presetArray.length)];
            this._map = gameV.add.tilemap(randomPreset);
            //Checks the Objectlayer for the following values, spawns them accordingly 
            var lastObjec;
            console.log(randomPreset);
            this._map.objects["ObjectLayer"].forEach(function (value) {
                console.log(randomPreset + " | " + value.type);
                //Coins
                if (value.type === "coin") {
                    var coin = new BenAndStan.Coin(gameV, 0, 0);
                    coin.Start(gameV.world.bounds.right + value.x, value.y);
                    lastObjec = coin;
                }
                //Seagull enemies
                if (value.type === "enemy_seagull") {
                    var seagull = new BenAndStan.EnemyMover(gameV, 0, 0);
                    seagull.Start(gameV.world.bounds.right + value.x, value.y);
                    lastObjec = seagull;
                }
                //Drone
                if (value.type === "enemy_drone") {
                    var drone = new BenAndStan.EnemyDrone(gameV, 0, 0);
                    drone.Start(gameV.world.bounds.right + value.x, value.y);
                    lastObjec = drone;
                }
                //Coin Magnet powerup
                if (value.type === "powerup_coinMagnet") {
                    var coinMag = new BenAndStan.CoinMagnet(gameV, 0, 0);
                    coinMag.Start(gameV.world.bounds.right + value.x, value.y, BenAndStan.Game.objectPool);
                }
                //Block powerup
                if (value.type === "powerup_block") {
                    var bat = new BenAndStan.BlockPowerUp(gameV, 0, 0);
                    bat.Start(gameV.world.bounds.right + value.x, value.y);
                }
            });
            console.log(randomPreset);
            lastObjec.EnableSpawnOnDestroy(); //Enables next preset spawn on last object
        }
    }
    BenAndStan.PrefabSpawner = PrefabSpawner;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * The Boot only loads in the splash and loading bar
     */
    class Boot extends Phaser.State {
        /**
         * Preloads the loader for the loading menu
         */
        preload() {
            this.load.image('preloadBar', 'assets/loader.png');
            this.load.image('splash', 'assets/Splash/level.jpg');
        }
        create() {
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;
            this.game.state.start('Preload');
        }
    }
    BenAndStan.Boot = Boot;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * The main game world class Gamestate creates all the main classes
     */
    class GameState extends Phaser.State {
        /**
         * Sets up the main classes for the game on entry of the main game
         */
        preload() {
            this._objectPool = new BenAndStan.ObjectPool(this.game);
            this._playerController = new BenAndStan.PlayerController(this.game, 0, 0);
            this._score = new BenAndStan.Score(this.game);
            this._vehicleController = new BenAndStan.VehicleController(this.game, 0, 0);
            this._pauseMenu = new BenAndStan.PauseMenu;
            this._presetSpawner = new BenAndStan.PrefabSpawner(this.game);
            //TEMP
            this._backgroundController = new BenAndStan.BackGroundController(this.game);
        }
        /**
         * Creates additional classes and starts certain classes
         */
        create() {
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this._playerController.CreatePlayer();
            this._vehicleController.Start(1100, 650);
            this._backgroundController.create();
            this._goldController = new BenAndStan.GoldController(this.game);
            this._goldController.create();
            this._objectPool.CreateList();
            this._score.Create();
            this._particleController = new BenAndStan.ParticleController(this.game);
        }
        /**
         * Updates certain classes which need manual updating
         */
        update() {
            this._backgroundController.MoveAllLayers();
            this._score.AddScore(0.2);
        }
    }
    BenAndStan.GameState = GameState;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * The Preload class loads all the files (images files, Json files and Audio files)
     */
    class Preload extends Phaser.State {
        constructor() {
            super(...arguments);
            /**
             * Importing google fonts4
             * Use the keys given below for the fonts
             */
            this.WebFontConfig = {
                function() { this.game.time.events.add(Phaser.Timer.SECOND, this.create, this); },
                google: {
                    families: ['ArcadeClassic', 'Big John', 'Slim Joe']
                }
            };
        }
        preload() {
            this._splash = this.add.sprite(0, 0, 'splash');
            this._splash.width = this.game.world.width;
            this._splash.height = this.game.world.height;
            this._preloadBar = this.add.sprite(200, 250, 'preloadBar');
            this.load.setPreloadSprite(this._preloadBar);
            //Load all the functions that contain files that need to be loaded
            this.LoadAllImages();
            this.LoadFiles();
            this.LoadAudio();
            // Load google webfont loader
            this.game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
        }
        /**
         * Load all the "Images" here
         * Sprites/Spritesheets ect.
         */
        LoadAllImages() {
            //Placeholders
            this.game.load.image('boat', 'assets/ProtoStan.png'); //Placeholder boat
            this.game.load.image('blackPixel', 'assets/OneWhitePixel.png');
            //Boosts
            this.game.load.image('boost', 'assets/Boosts/Boost.png');
            this.game.load.image('hitter', 'assets/Boosts/Hitter.png');
            this.game.load.image('coinNet', 'assets/Boosts/Coinnet.png');
            this.game.load.image('coinMulti', 'assets/Boosts/Coinx2.png');
            this.game.load.image('coinMagnet', 'assets/Boosts/CoinMagnet.png');
            //Obstacles
            this.game.load.image('drone', 'assets/Obstacles/Drone.png');
            // --- Parallax objects --- \\
            /*
                the parallax is layered in 5 layers in total
                Going from -01 till 03 + a player layer and a main background
            */
            // -- Lake -- \\
            //Main background
            this.game.load.image('skyMain', 'assets/Parallax/skylake_paralax_complete_background.png');
            //Start round phase objects
            this.game.load.image('bgHouse', 'assets/Parallax/startlevel_02_layer02.png');
            this.game.load.image('mainStart', 'assets/Parallax/startlevel_01_layer_playerline.png');
            this.game.load.image('mountain01', 'assets/Parallax/mountain_01_paralax_03.png');
            // Parallax 03
            this.game.load.image('mountain01', 'assets/Parallax/mountain_01_paralax_03.png');
            this.game.load.image('mountain02', 'assets/Parallax/mountain_02_paralax_03.png');
            this.game.load.image('mountain03', 'assets/Parallax/mountain_03_paralax_03.png');
            this.game.load.image('mountain04', 'assets/Parallax/mountain_04_paralax_03.png');
            this.game.load.image('mountain05', 'assets/Parallax/mountain_05_paralax_03.png');
            this.game.load.image('mountain06', 'assets/Parallax/mountain_06_paralax_03.png');
            // Parallax 02
            this.game.load.image('Object0102', 'assets/Parallax/boei_02_paralax_02.png');
            this.game.load.image('Object0202', 'assets/Parallax/island_small_paralax_layer_02.png');
            this.game.load.image('Object0302', 'assets/Parallax/islandbirdsign_small_paralax_layer_02.png');
            // Parallax 01
            this.game.load.image('Object0101', 'assets/Parallax/boei_01_paralax_01.png');
            this.game.load.image('Object0201', 'assets/Parallax/islandbirdsign_small_paralax_layer_01.png');
            this.game.load.image('Object0301', 'assets/Parallax/island_small_paralax_layer_01.png');
            this.game.load.image('Object0401', 'assets/Parallax/island_big_paralax_layer_01.png');
            // Parallax -01
            this.game.load.image('Object0100', 'assets/Parallax/forground_01_layer-01.png');
            this.game.load.image('Object0200', 'assets/Parallax/forground_02_layer-01.png');
            this.game.load.image('Object0300', 'assets/Parallax/forground_03_layer-01.png');
            this.game.load.image('Object0400', 'assets/Parallax/forground_04_layer-01.png');
            //Ingame Backgrounds
            this.game.load.image('backgroundStart', 'assets/backgrounds/lake_background_start.png');
            this.game.load.image('background1', 'assets/backgrounds/lake_background_01.png');
            this.game.load.image('background2', 'assets/backgrounds/lake_background_02.png');
            this.game.load.image('background3', 'assets/backgrounds/lake_background_03.png');
            this.game.load.image('background4', 'assets/backgrounds/lake_background_04.png');
            this.game.load.image('background5', 'assets/backgrounds/lake_background_05.png');
            this.game.load.image('background6', 'assets/backgrounds/lake_background_06.png');
            this.game.load.image('background7', 'assets/backgrounds/lake_background_07.png');
            this.game.load.image('background8', 'assets/backgrounds/lake_background_08.png');
            //Spritesheets
            this.game.load.spritesheet('player', 'assets/SpriteSheets/Stan_Idle_WIP.png', 389, 474, 30); //Placeholder
            this.game.load.spritesheet('ben_fall_01', 'assets/SpriteSheets/ben_fall_01.png', 487, 510, 16);
            this.game.load.spritesheet('ben_fall_02', 'assets/SpriteSheets/ben_fall_02.png', 487, 510, 16);
            this.game.load.spritesheet('ben_fall_03', 'assets/SpriteSheets/ben_fall_03.png', 487, 510, 5);
            this.game.load.spritesheet('player_hit', 'assets/SpriteSheets/StanHITBAT.png', 463, 533, 30);
            this.game.load.spritesheet('coin', 'assets/SpriteSheets/Coin_Spritesheet.png', 54, 54);
            this.game.load.spritesheet('ben_hit_01', 'assets/SpriteSheets/ben_idle_bat_01.png', 314, 453, 24);
            this.game.load.spritesheet('ben_hit_02', 'assets/SpriteSheets/ben_idle_bat_02.png', 314, 453, 6);
            this.game.load.spritesheet('stan_selfie', 'assets/SpriteSheets/Stan_Selfie.png', 250, 112, 54);
            this.game.load.spritesheet('stan_startup', 'assets/SpriteSheets/Stan_Startup.png', 250, 112, 23);
            this.game.load.spritesheet('seagull_idle', 'assets/SpriteSheets/Seagull_Idle.png', 98, 108, 15);
            this.game.load.spritesheet('seagull_hit', 'assets/SpriteSheets/Seagull_Hit.png', 98, 108, 14);
            this.game.load.spritesheet('drone_idle', 'assets/SpriteSheets/Drone_Idle.png', 150, 83, 20);
            this.game.load.spritesheet('drone_hit', 'assets/SpriteSheets/Drone_Hit.png', 226, 241, 20);
            //VFX Spritesheets
            this.game.load.spritesheet('boat_foam01', 'assets/VFX/boat_foam_vfx_01.png', 720, 112, 41);
            this.game.load.spritesheet('boat_foam02', 'assets/VFX/boat_foam_vfx_02.png', 720, 112, 19);
            this.game.load.spritesheet('waterSplashVFX', 'assets/VFX/water_splash_vfx.png', 300, 400, 24);
            this.game.load.spritesheet('boost01VFX', 'assets/VFX/boost_pickup_big_01.png', 500, 500, 16);
            this.game.load.spritesheet('boost02VFX', 'assets/VFX/boost_pickup_big_02.png', 2048, 2048, 9);
            this.game.load.spritesheet('boostMidVFX', 'assets/VFX/boost_pickup_mid.png', 2048, 2048, 25);
            this.game.load.spritesheet('boostSVFX', 'assets/VFX/boost_pickup_small.png', 2048, 2048, 25);
            this.game.load.spritesheet('coinMagVFX', 'assets/VFX/coin_magnet_circle_vfx.png', 400, 400, 25);
            this.game.load.spritesheet('coinPVFX', 'assets/VFX/coin_pickup_vfx.png', 54, 54, 28);
            this.game.load.spritesheet('fallVFX', 'assets/VFX/fall_vfx.png', 300, 300, 18);
            //UI
            //Main menu
            this.game.load.image('mainTitle', 'assets/main_menu/title.png'); //Title
            this.game.load.image('startButton', 'assets/main_menu/start_button.png');
            this.game.load.image('highButton', 'assets/main_menu/highscore_button.png');
            this.game.load.image('optionsButton', 'assets/main_menu/options_button.png');
            this.game.load.image('shopButton', 'assets/main_menu/shop_button.png');
            //Options
            this.game.load.image('optionsText', 'assets/option_menu/options_text.png'); //Title + Text
            this.game.load.image('backButton', 'assets/option_menu/back_button.png');
            this.game.load.image('volumeEmpty', 'assets/option_menu/volume_bar_empty.png');
            this.game.load.image('volumeFull', 'assets/option_menu/volume_bar_full.png');
            this.game.load.image('volumeHandle', 'assets/option_menu/volume_handle.png');
            //Highscores
            this.game.load.image('highscoreTitle', 'assets/highscore_menu/highscore_title.png'); //Title
            //pause screen
            this.game.load.image('pauseTitle', 'assets/pause_screen/bucketlist.png'); //Title
            this.game.load.image('screenshotButton', 'assets/pause_screen/camera_button.png');
            this.game.load.image('skipButton', 'assets/pause_screen/skip_button.png');
            this.game.load.image('pauseButton', 'assets/pause_screen/pause_button.png'); //Ingame pausebutton
            //UI Other
            this.game.load.image('title', 'assets/main_menu/title.png');
            this.game.load.image('filter', 'assets/main_menu/filter.png');
            this.game.load.image('pauseFilter', 'assets/pause_screen/filter_overlay.png');
        }
        /**
         * Preload all the "Files" here
         */
        LoadFiles() {
            //Json Files
            this.game.load.tilemap('preset01', 'Data/Coin_preset_01.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset02', 'Data/Coin_preset_02.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset03', 'Data/Coin_preset_03.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset04', 'Data/Coin_preset_04.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset05', 'Data/Coin_preset_05.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset06', 'Data/Coin_preset_06.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('presetRW01', 'Data/risk_reward_01.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('presetRW02', 'Data/risk_reward_02.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset07', 'Data/Coin_preset_07.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset08', 'Data/Coin_preset_08.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset09', 'Data/Coin_preset_09.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset10', 'Data/Risk_reward_01.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset11', 'Data/Risk_reward_02.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset12', 'Data/Risk_reward_03.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset13', 'Data/Risk_reward_04.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset14', 'Data/Risk_preset_01.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset15', 'Data/4_drones.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset16', 'Data/seagull_curve.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset17', 'Data/yer_wizard_harry.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset18', 'Data/coinmagent_arrow.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset19', 'Data/coins_mid.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset20', 'Data/curving_synth.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset21', 'Data/seagulls_mid_coinmagnet_front.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset22', 'Data/twodrones_onecoin.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset23', 'Data/twoseagulls_onedrone_rightside_map.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('preset24', 'Data/CoinEnemyBlock.json', null, Phaser.Tilemap.TILED_JSON);
        }
        /**
         * Load all "Audio" files here
         */
        LoadAudio() {
            //Audio files
            this.game.load.audio('BGM_Sea', 'Audio/BGM_Sea.mp3');
            this.game.load.audio('click_sfx', 'Audio/click_sfx.wav');
            this.game.load.audio('coin_sfx', 'Audio/coin_sfx.wav');
            this.game.load.audio('death_ambient', 'Audio/death_ambient.mp3');
            this.game.load.audio('hit_sfx', 'Audio/hit_sfx.mp3');
            this.game.load.audio('jump_sfx', 'Audio/jump_sfx.wav');
            this.game.load.audio('powerup_sfx', 'Audio/powerup_sfx.wav');
        }
        create() {
            this.CreateLoadingBar();
        }
        /**
         * Creating a loading bar
         */
        CreateLoadingBar() {
            var tween = this.add.tween(this._preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.FadeToGame, this);
        }
        /**
         *  Fades the splash screen into the menu/game
         */
        FadeToGame() {
            var tween = this.add.tween(this._splash).to({ alpha: 0 }, 750, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.StartMainGame, this);
        }
        /**
         * Start the menu stage
         */
        StartMainGame() {
            this.game.state.start("PrepMenu", true, false);
        }
    }
    BenAndStan.Preload = Preload;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * The PauseMenu class controls the game so that the game can be paused.
     */
    class PauseMenu {
        /**
         * Creates the Pause Menu
         */
        create() {
            //Create Button
            this._pauseButton = this.game.add.button(this.game.world.centerX, this.game.world.centerY, 'pauseButton', this.PauseGame, this);
            //Create pause screen		
            // CHANGE FOR Objectpool
            this._filter = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.height, 'pauseFilter');
            this._pauseTitle = this.game.add.sprite(this._filter.x, this._filter.y - 20, 'pauseTitle');
            this._menuButton = this.game.add.button(this._filter.x - 5, this._filter.y, 'skipButton'); //Placeholder art
            //Anchors
            //Adding to group
            this._pauseGroup.add(this._filter);
            this._pauseGroup.add(this._menuButton);
            this._pauseGroup.add(this._pauseTitle);
            this._pauseGroup.add(this._resumeButton);
        }
        /**
         * Pauses the game
         */
        PauseGame() {
            this._pauseButton.visible = false;
            this.SlideIn();
        }
        /**
         * Slides the menu in
         */
        SlideIn() {
            var bounce = this.game.add.tween(this._pauseGroup);
            bounce.to(this.game.width, 3, Phaser.Easing.Bounce.In);
        }
        /**
         * Slides the menu out
         */
        SlideOut() {
            var bounce = this.game.add.tween(this._pauseGroup);
            bounce.to(this.game.width, 3, Phaser.Easing.Bounce.In);
        }
    }
    BenAndStan.PauseMenu = PauseMenu;
})(BenAndStan || (BenAndStan = {}));
var BenAndStan;
(function (BenAndStan) {
    /**
     * The PrepMenu is the main menu where you can start the game from here.
     */
    class PrepMenu extends Phaser.State {
        constructor() {
            super(...arguments);
            this._fadeCompleted = false;
        }
        /**
         * Creates the main Menu
         */
        create() {
            this._background = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'background2');
            this._filter = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'filter');
            this._title = this.game.add.sprite(this.game.world.centerX - 436, this.world.centerY - 350, 'mainTitle');
            //Setting Buttons
            this._startButton = this.game.add.button(this.game.world.centerX - 110, this.game.world.centerY - 70, 'startButton', this.StartGame, this);
            this._highscoreButton = this.game.add.button(this.game.world.centerX - 200, this.game.world.centerY + 30, 'highButton', this.ShowHighscore, this, this._highscoreButton, this._optionsButton);
            this._optionsButton = this.game.add.button(this.game.world.centerX - 152, this.game.world.centerY + 120, 'optionsButton', this.ShowOptions, this);
            this._background.alpha = 0;
            var tween = this.game.add.tween(this._background).to({ alpha: 1 }, 750, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.EnabledFadeCompleted, this);
            this._filter.alpha = 0;
            var tween = this.game.add.tween(this._filter).to({ alpha: 1 }, 750, Phaser.Easing.Linear.None, true);
            this._title.alpha = 0;
            var tween = this.game.add.tween(this._title).to({ alpha: 1 }, 750, Phaser.Easing.Linear.None, true);
            this._startButton.alpha = 0;
            var tween = this.game.add.tween(this._startButton).to({ alpha: 1 }, 750, Phaser.Easing.Linear.None, true);
            this._highscoreButton.alpha = 0;
            var tween = this.game.add.tween(this._highscoreButton).to({ alpha: 1 }, 750, Phaser.Easing.Linear.None, true);
            this._optionsButton.alpha = 0;
            var tween = this.game.add.tween(this._optionsButton).to({ alpha: 1 }, 750, Phaser.Easing.Linear.None, true);
            this.game.sound.play('BGM_Sea', 1, true);
        }
        /**
         * Enables the FadeCompleted boolean when the menu finished its fade
         */
        EnabledFadeCompleted() {
            this._fadeCompleted = true;
        }
        /**
         * Starts the main game world on start button press
         */
        StartGame() {
            if (this._fadeCompleted) {
                this.game.sound.play('click_sfx', 1, false);
                this.game.state.start("GameState");
            }
        }
        ButtonBlink() {
        }
        ShowHighscore() {
        }
        ShowOptions() {
        }
    }
    BenAndStan.PrepMenu = PrepMenu;
})(BenAndStan || (BenAndStan = {}));
//# sourceMappingURL=app.js.map