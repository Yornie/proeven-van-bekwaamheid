﻿module BenAndStan
{
	export class ObjectPool
	{
		private _game: Game;

		private _coinList: Coin[];
		private _prefabSpawner: PrefabSpawner;
		private _bgController: BackGroundController;

		/**
		 * Constructs the Object Pool class
		 * @param game sets the _game variable according to the given parameter
		 */
		constructor(game: Game)
		{
			this._game = game;
			Game.objectPool = this;
			this._prefabSpawner = new PrefabSpawner(this._game);
			this._bgController = new BackGroundController(this._game);
		}

		/**
		 * Creates coin list for the pool
		 */
		public CreateList(): void
		{
			this._coinList = new Array<Coin>();
			this._prefabSpawner.spawnPreset();
		}

		/**
		 * Toggles attract for the coins in the coin list
		 * @param enabled toggles the Attract variables in the coins according to the given parameter
		 */
		public ToggleAttract(enabled : boolean): void
		{
			this._coinList.forEach(function (coin)
			{
				if (coin.AttractEnabled() != enabled)
				{
					coin.ToggleAttract(enabled);
				}
			});
		}

		/**
		 * Adds coin to the coin list
		 * @param coin adds coin to the _coinList variable
		 */
		public AddCoin(coin: Coin): void
		{
			this._coinList.push(coin);
		}
	}
}