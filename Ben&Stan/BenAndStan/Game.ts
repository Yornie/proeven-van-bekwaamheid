﻿module BenAndStan
{
	/**
	 * This class creates the game engine throught HTML5 in the browser
	 */
	export class Game extends Phaser.Game
	{
		/**
		 * Constructs the initial costructor of the gamne engine
		 */
		constructor()
		{
			super(1280, 720, Phaser.AUTO, 'content');
			Game.game = this;

			//Adds states to the game
			this.state.add("Boot", Boot, false);
			this.state.add("Preload", Preload, false);
			this.state.add("PrepMenu", PrepMenu, false);
			this.state.add("GameState", GameState, false);
			this.state.start("Boot");
		}

		//Global variables
		static constants: Constants;
		static player: PlayerController;
		static vehicle: VehicleController;
		static game: Game;
		static goldController: GoldController;
		static objectPool: ObjectPool;
		static backgroundController: BackGroundController;
		static prefabSpawner: PrefabSpawner;
		static particleController: ParticleController;
		static score: Score;
	}
}